#!/usr/bin/env bash
clear
#!/bin/bash
sh /opt/ma-sawtooth/scripts/stop.sh

## Remove the  LMDB files
echo "Removing LMDB Files ..."
rm /var/lib/sawtooth/*lmdb*
## Remove the chain Id.
echo "Removing chain Id ..."
rm /var/lib/sawtooth/block-chain-id
## cd ~
## set up  new keys for user
echo "Overwriting user with new keys ...."
sawtooth keygen --force
echo "Overwriting Validator with new keys ..."
sawadm keygen --force

echo "Genesis Config Batch"
sawset genesis -k /etc/sawtooth/keys/validator.priv -o /opt/ma-sawtooth/batches/config-genesis.batch

echo "PoET Config Batch"
sawset proposal create -k /etc/sawtooth/keys/validator.priv \
-o /opt/ma-sawtooth/batches/config.batch \
sawtooth.consensus.algorithm=poet \
sawtooth.poet.report_public_key_pem="$(cat /etc/sawtooth/simulator_rk_pub.pem)" \
sawtooth.poet.valid_enclave_measurements=$(poet enclave measurement) \
sawtooth.poet.valid_enclave_basenames=$(poet enclave basename)

echo "Poet Batch"
sudo -u sawtooth poet registration create -k /etc/sawtooth/keys/validator.priv -o /opt/ma-sawtooth/batches/poet.batch

echo "Poet Proposal"
sawset proposal create -k /etc/sawtooth/keys/validator.priv \
-o /opt/ma-sawtooth/batches/poet-settings.batch \
sawtooth.poet.target_wait_time=10 \
sawtooth.poet.initial_wait_time=20 \
sawtooth.publisher.max_batches_per_block=100 \
sawtooth.poet.ztest_minimum_win_count=10000000000000000 \
sawtooth.poet.key_block_claim_limit=100000000000000000

echo "Genesis Erzeugen"
sawadm genesis /opt/ma-sawtooth/batches/config-genesis.batch \
/opt/ma-sawtooth/batches/config.batch \
/opt/ma-sawtooth/batches/poet.batch \
/opt/ma-sawtooth/batches/poet-settings.batch

sh /opt/ma-sawtooth/scripts/start.sh