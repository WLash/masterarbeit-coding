#!/usr/bin/env bash
sudo -u sawtooth sawtooth-validator \
--peers dynamic \
--bind component:tcp://127.0.0.1:4004 \
--bind network:tcp://192.168.8.107:8800 \
--endpoint tcp://192.168.8.107:8800 \
--scheduler parallel \
--seeds tcp://192.168.8.108:8800,tcp://192.168.8.109:8800,tcp://192.168.8.110:8800 -v &

P1=$!
sleep .5
sudo -u sawtooth sawtooth-rest-api -t 5 -v &
P2=$!
sleep .5
sudo -u sawtooth settings-tp -v &
P3=$!
sleep .5
sudo -u sawtooth poet-validator-registry-tp -v &
P4=$!

sleep .5
sudo java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044 \
-cp /opt/ma-sawtooth/java/ma-sawtooth-1.0-SNAPSHOT.jar it.laschinger.sawtooth.transactions.user.UserProcessor tcp://localhost:4004 &
P5=$!

sleep .5
sudo java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1045 \
-cp /opt/ma-sawtooth/java/ma-sawtooth-1.0-SNAPSHOT.jar it.laschinger.sawtooth.transactions.tariff.TariffProcessor tcp://localhost:4004 &
P6=$!

sleep .5
sudo java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1046 \
-cp /opt/ma-sawtooth/java/ma-sawtooth-1.0-SNAPSHOT.jar it.laschinger.sawtooth.transactions.usage.UsageProcessor tcp://localhost:4004  &
P7=$!

sleep .5
sudo java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1047 \
-cp /opt/ma-sawtooth/java/ma-sawtooth-1.0-SNAPSHOT.jar it.laschinger.sawtooth.transactions.accounting.AccountingProcessor tcp://localhost:4004
P8=$!

wait $P1 $P2 $P3 $P4 $P5 $P6 $P7 $P8