package it.laschinger.sawtooth.domain.creation;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

public class Creation extends CreationDbEntity{

	@JsonProperty
	private String title;

	@JsonProperty
	private CreationUser owner;

	@JsonProperty
	private Set<CreationShare> shareSet = new HashSet<>();

	public Creation(){}

	public Creation(final String title, final CreationUser owner){
		this.title = title;
		this.owner = owner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<CreationShare> getShareSet() {
		return shareSet;
	}

	public void setShareSet(Set<CreationShare> shareSet) {
		this.shareSet = shareSet;
	}

	public CreationUser getOwner() {
		return owner;
	}

	public void setOwner(CreationUser owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Creation{" +
				"id=" + getId() +
				", owner=" + owner +
				", title='" + title + '\'' +
				", shareSet=" + shareSet +
				'}';
	}
}
