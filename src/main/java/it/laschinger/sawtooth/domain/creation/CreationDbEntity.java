package it.laschinger.sawtooth.domain.creation;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreationDbEntity {

	private final static Logger logger = LoggerFactory.getLogger(CreationDbEntity.class);

	@JsonProperty
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof CreationDbEntity) {
			CreationDbEntity entity = (CreationDbEntity) obj;
			return id.equals(entity.getId());
		}else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + id.hashCode();
		return hash;
	}
}
