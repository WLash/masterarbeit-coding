package it.laschinger.sawtooth.domain.creation;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CreationShare extends CreationDbEntity{

	@JsonProperty
	private Creation creation;

	@JsonProperty
	private CreationUser shareHolder;

	@JsonProperty
	private BigDecimal quotient;

	public CreationShare(){}

	public CreationShare(final Creation creation, final CreationUser shareHolder, final BigDecimal quotient){
		this.creation = creation;
		this.shareHolder = shareHolder;
		this.quotient = quotient;
	}

	public CreationUser getShareHolder() {
		return shareHolder;
	}

	public void setShareHolder(CreationUser shareHolder) {
		this.shareHolder = shareHolder;
	}

	public BigDecimal getQuotient() {
		return quotient;
	}

	public void setQuotient(BigDecimal quotient) {
		this.quotient = quotient;
	}

	public Creation getCreation() {
		return creation;
	}

	public void setCreation(Creation creation) {
		this.creation = creation;
	}

	@Override
	public String toString() {
		return "CreationShare{" +
				"id=" + getId() +
				", creation=" + creation +
				", shareHolder=" + shareHolder +
				", quotient=" + quotient +
				'}';
	}
}