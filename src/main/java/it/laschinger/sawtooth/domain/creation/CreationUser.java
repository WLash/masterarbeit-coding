package it.laschinger.sawtooth.domain.creation;

public class CreationUser extends CreationDbEntity{

	private String name;

	public CreationUser(){}

	public CreationUser(final String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CreationUser{" +
				"id=" + getId() +
				", name='" + name + '\'' +
				'}';
	}
}
