package it.laschinger.sawtooth.domain.entities;

import java.util.List;

public interface EntityI {
	List<AddressField> sortedAdressFields();
	<T extends Entity&EntityI> T deserializePayload(String payload);
	String serializePayload();
}
