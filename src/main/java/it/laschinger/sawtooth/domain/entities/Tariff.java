package it.laschinger.sawtooth.domain.entities;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import it.laschinger.sawtooth.domain.entities.protobuf.entities.EntityProtos;
import it.laschinger.sawtooth.transactions.tariff.TariffHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Tariff extends Entity implements EntityI {

	private final static Logger logger = LoggerFactory.getLogger(Tariff.class);
	private final static String OBJ_IDENTIFIER = "0";

	private String distributorAddress = null;

	private String royaltyOrganisationAddress = null;

	private Usage.Type usageType = null;

	private BigDecimal amount = null;

	public Tariff(){
		super(TariffHandler.FAMILY_NAME, OBJ_IDENTIFIER);
	}

	public Tariff(final String distributorAddress, final String royaltyOrganisationAddress, final Usage.Type usageType, final BigDecimal amount){
		this();
		this.distributorAddress = distributorAddress;
		this.royaltyOrganisationAddress = royaltyOrganisationAddress;
		this.usageType = usageType;
		this.amount = amount;
	}

	public Tariff(final String payload){
		this();
		final Tariff tariff = deserializePayload(payload);
		this.distributorAddress = tariff.getDistributorAddress();
		this.royaltyOrganisationAddress = tariff.getRoyaltyOrganisationAddress();
		this.usageType = tariff.getUsageType();
		this.amount = tariff.getAmount();
	}

	public String getDistributorAddress() {
		return distributorAddress;
	}

	public void setDistributorAddress(String distributorAddress) {
		this.distributorAddress = distributorAddress;
	}

	public String getRoyaltyOrganisationAddress() {
		return royaltyOrganisationAddress;
	}

	public void setRoyaltyOrganisationAddress(String royaltyOrganisationAddress) {
		this.royaltyOrganisationAddress = royaltyOrganisationAddress;
	}

	public Usage.Type getUsageType() {
		return usageType;
	}

	public void setUsageType(Usage.Type usageType) {
		this.usageType = usageType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public List<AddressField> sortedAdressFields() {
		final List<AddressField> fields = new ArrayList<>();
		fields.add(new AddressField(0, 4, () -> this.usageType.name(), true));
		fields.add(new AddressField(0, 29, ()-> this.distributorAddress.substring(40, 70), false));
		fields.add(new AddressField(0, 30, ()-> this.royaltyOrganisationAddress.substring(40, 70), false));
		return fields;
	}

	@Override
	public Tariff deserializePayload(String payload) {
		final ByteString byteString = ByteString.copyFromUtf8(payload);
		try {
			final EntityProtos.Tariff protoTariff = EntityProtos.Tariff.parseFrom(byteString);

			final Tariff tariff = new Tariff(protoTariff.getDistributorAddress(), protoTariff.getRoyaltyOrganisationAddress(),
					Usage.Type.valueOf(protoTariff.getUsageType().name()), new BigDecimal(protoTariff.getAmount()));
			return tariff;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String serializePayload() {
		final EntityProtos.Tariff.Builder builder = EntityProtos.Tariff.newBuilder()
			.setDistributorAddress(this.distributorAddress).setRoyaltyOrganisationAddress(this.royaltyOrganisationAddress)
			.setUsageType(EntityProtos.Usage.Type.valueOf(this.usageType.name())).setAmount(this.amount.toString());

		final EntityProtos.Tariff protoTariff = builder.build();

		return serializeMessage(protoTariff);
	}
}
