package it.laschinger.sawtooth.domain.entities;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import it.laschinger.sawtooth.domain.entities.protobuf.entities.EntityProtos;
import it.laschinger.sawtooth.transactions.accounting.AccountingHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Invoice extends Entity implements EntityI {

	public final static String OBJ_IDENTIFIER = "0";

	private String invoiceNo;

	private String recipientAddress;

	private String originatorAddress;

	private State state;

	private BigDecimal amount;

	private List<String> feeAddresses;

	public Invoice(){
		super(AccountingHandler.FAMILY_NAME, OBJ_IDENTIFIER);
	}

	public Invoice(String invoiceNo, String recipientAddress, String originatorAddress, List<String> feeAddresses, State state, BigDecimal amount) {
		this();
		this.invoiceNo = invoiceNo;
		this.recipientAddress = recipientAddress;
		this.originatorAddress = originatorAddress;
		this.feeAddresses = feeAddresses;
		this.state = state;
		this.amount = amount;
	}

	public Invoice(final String payload){
		this();
		final Invoice invoice = deserializePayload(payload);
		this.invoiceNo = invoice.invoiceNo;
		this.recipientAddress = invoice.recipientAddress;
		this.originatorAddress = invoice.originatorAddress;
		this.feeAddresses = invoice.feeAddresses;
		this.state = invoice.state;
		this.amount = invoice.amount;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getRecipientAddress() {
		return recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public String getOriginatorAddress() {
		return originatorAddress;
	}

	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	public List<String> getFeeAddresses() {
		return feeAddresses;
	}

	public void setFeeAddresses(List<String> feeAddresses) {
		this.feeAddresses = feeAddresses;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public List<AddressField> sortedAdressFields() {
		final List<AddressField> fields = new ArrayList<>();
		fields.add(new AddressField(0, 21, () -> invoiceNo, true));
		fields.add(new AddressField(0, 21, () -> recipientAddress.substring(40, 70), false));
		fields.add(new AddressField(0, 21, () -> originatorAddress.substring(40, 70), false));

		return fields;
	}

	@Override
	public Invoice deserializePayload(String payload) {
		final ByteString byteString = ByteString.copyFromUtf8(payload);
		try {
			final EntityProtos.Invoice proto = EntityProtos.Invoice.parseFrom(byteString);

			final Invoice invoice = new Invoice(proto.getInvoiceNo(), proto.getRecipientAddress(),
				proto.getOriginatorAddress(), proto.getFeeAddressesList(),
				State.valueOf(proto.getState().name()), new BigDecimal(proto.getAmount()));
			return invoice;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String serializePayload() {
		final EntityProtos.Invoice.Builder builder = EntityProtos.Invoice.newBuilder()
			.setInvoiceNo(this.invoiceNo)
			.setRecipientAddress(this.recipientAddress)
			.setOriginatorAddress(this.originatorAddress)
			.addAllFeeAddresses(this.feeAddresses)
			.setState(EntityProtos.Invoice.State.valueOf(this.state.name()))
			.setAmount(this.amount.toString());

		final EntityProtos.Invoice proto = builder.build();
		return serializeMessage(proto);
	}


	@Override
	public String toString() {
		return "Invoice{" +
				"invoiceNo='" + invoiceNo + '\'' +
				", recipientAddress='" + recipientAddress + '\'' +
				", originatorAddress='" + originatorAddress + '\'' +
				", state=" + state +
				", amount=" + amount +
				", feeAddresses=" + feeAddresses +
				'}';
	}

	public enum State{
		Open, Payed
	}
}
