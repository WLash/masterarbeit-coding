package it.laschinger.sawtooth.domain.entities;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Entity {

	private final static Logger logger = LoggerFactory.getLogger(Entity.class);

	private final String familyName;
	private final String objIdentifier;
	private String requestPayload;
	private String transactionHeader;
	private List<String> dependencies = new ArrayList<>();

	public Entity(final String familyName, final String objIdentifier){
		this.familyName = familyName;
		this.objIdentifier = objIdentifier;
	}

	public String getFamilyName() {
		return familyName;
	}

	public String getObjIdentifier() {
		return objIdentifier;
	}

	public String serializeMessage(final GeneratedMessageV3 message){
		final ByteString.Output output = ByteString.newOutput();
		try {
			message.writeTo(output);
			final String result = output.toByteString().toStringUtf8();
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getRequestPayload() {
		return requestPayload;
	}

	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}

	public String getTransactionHeader() {
		return transactionHeader;
	}

	public void setTransactionHeader(String transactionHeader) {
		this.transactionHeader = transactionHeader;
	}

	public List<String> getDependencies() {
		return dependencies;
	}

	public boolean addDependency(final String dependency){
		return dependencies.add(dependency);
	}

	public void addDependencies(final String... dependencies){
		Arrays.stream(dependencies).forEach(this::addDependency);
	}
}
