package it.laschinger.sawtooth.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import it.laschinger.sawtooth.domain.entities.protobuf.entities.EntityProtos;
import it.laschinger.sawtooth.transactions.user.UserHandler;
import it.laschinger.sawtooth.utils.GlobalUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class User extends Entity implements EntityI {

	private final static Logger logger = LoggerFactory.getLogger(User.class);
	public static String OBJ_IDENTIFIER = "0";

	private String publicKeyHex;

	private String name;

	private BigDecimal balance;

	private State state;

	private Role role;

	private String addressRoyaltyOrganisation;

	private String creationDbId;

	public User(){
		super(UserHandler.FAMILY_NAME, OBJ_IDENTIFIER);
	}

	public User(final String payload){
		this();
		final User user = deserializePayload(payload);
		this.publicKeyHex = user.getPublicKeyHex();
		this.name = user.getName();
		this.balance = user.getBalance();
		this.state = user.getState();
		this.role = user.getRole();
		this.addressRoyaltyOrganisation = user.getAddressRoyaltyOrganisation();
		this.creationDbId = user.getCreationDbId();
	}

	public User(final String publicKeyHex, final String name, final BigDecimal balance, final State state, final Role role, final String addressRoyaltyOrganisation, final String creationDbId){
		this();
		this.publicKeyHex = publicKeyHex;
		this.name = name;
		this.balance = balance;
		this.state = state;
		this.role = role;
		this.addressRoyaltyOrganisation = addressRoyaltyOrganisation;
		this.creationDbId = creationDbId;
	}

	public String getPublicKeyHex() {
		return publicKeyHex;
	}

	public void setPublicKeyHex(String publicKeyHex) {
		this.publicKeyHex = publicKeyHex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getAddressRoyaltyOrganisation() {
		return addressRoyaltyOrganisation;
	}

	public void setAddressRoyaltyOrganisation(String addressRoyaltyOrganisation) {
		this.addressRoyaltyOrganisation = addressRoyaltyOrganisation;
	}

	public String getCreationDbId() {
		return creationDbId;
	}

	public void setCreationDbId(String creationDbId) {
		this.creationDbId = creationDbId;
	}

	@Override
	public List<AddressField> sortedAdressFields() {
		final List<AddressField> fields = new ArrayList<>();
		fields.add(new AddressField(0, 4, () -> this.role.name(), true));
		fields.add(new AddressField(0, 29, () -> this.creationDbId == null || this.creationDbId.isEmpty() ? "00000000000000000000000000000" : GlobalUtils.hashString(this.creationDbId), false));
		fields.add(new AddressField(0, 30, ()-> this.publicKeyHex, true));

		return fields;
	}

	@JsonIgnore
	public boolean isArtist(){ return this.role==Role.ARTIST; }

	@JsonIgnore
	public boolean isPublisher(){ return this.role==Role.PUBLISHER; }
	@JsonIgnore
	public boolean isDistributor(){ return this.role==Role.DISTRIBUTOR; }
	@JsonIgnore
	public boolean isRoyaltyOrganisation(){ return this.role==Role.ROYALTY_ORGANISATION; }



	public enum Role{
		ROYALTY_ORGANISATION,
		DISTRIBUTOR,
		ARTIST,
		PUBLISHER;

		Role(){}
	}

	public enum State{
		INQUIRED, CONFIRMED, DECLINED;

		State(){}
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + publicKeyHex.hashCode();
		return hash;
	}

	@Override
	public String toString() {
		return "User{" +
				"publicKeyHex='" + publicKeyHex + '\'' +
				", name='" + name + '\'' +
				", balance=" + balance +
				", state=" + state +
				", role=" + role +
				", addressRoyaltyOrganisation='" + addressRoyaltyOrganisation + '\'' +
				", creationDbId=" + creationDbId +
				'}';
	}

	@Override
	public User deserializePayload(String payload) {
		final ByteString byteString = ByteString.copyFromUtf8(payload);
		try {
			final EntityProtos.User proto =
				EntityProtos.User.parseFrom(byteString);
			final User user = new User(proto.getPubKey(), proto.getName(),
				proto.getBalance().isEmpty() ?
					BigDecimal.ZERO :
					new BigDecimal(proto.getBalance()),
				State.valueOf(proto.getState().name()),
				Role.valueOf(proto.getRole().name()),
				proto.getRoyaltyOrganisationAddress(),
				proto.getCreationDbUserId());
			return user;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String serializePayload() {
		final EntityProtos.User.Builder builder =
				EntityProtos.User.newBuilder()
			.setPubKey(this.publicKeyHex)
			.setName(this.name)
			.setBalance(this.balance.toString())
			.setState(EntityProtos.User.State.valueOf(this.state.name()))
			.setRole(EntityProtos.User.Role.valueOf(this.role.name()));
		if(this.getAddressRoyaltyOrganisation() != null){
			builder
				.setRoyaltyOrganisationAddress(this.addressRoyaltyOrganisation);
		}
		if(this.creationDbId != null){
			builder.setCreationDbUserId(this.creationDbId);
		}
		final EntityProtos.User protoUser = builder.build();
		return serializeMessage(protoUser);
	}
}
