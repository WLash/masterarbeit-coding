package it.laschinger.sawtooth.domain.entities;

import it.laschinger.sawtooth.utils.GlobalUtils;

import java.util.function.Supplier;

public class AddressField {
	private int start;
	private int end;
	private Supplier<String> stringSupplier;
	private boolean hasToBeHashed;

	AddressField(final int start, final int end,
				 final Supplier<String> stringSupplier,
				 final boolean hasToBeHashed) {
		this.start = start;
		this.end = end;
		this.stringSupplier = stringSupplier;
		this.hasToBeHashed = hasToBeHashed;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public Supplier<String> getStringSupplier() {
		return stringSupplier;
	}

	public boolean hasToBeHashed() {
		return hasToBeHashed;
	}

	public String getValue() {
		return hasToBeHashed ?
			GlobalUtils.hashString(stringSupplier.get()) :
			stringSupplier.get();
	}
}
