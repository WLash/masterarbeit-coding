package it.laschinger.sawtooth.domain.entities;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import it.laschinger.sawtooth.domain.entities.protobuf.entities.EntityProtos;
import it.laschinger.sawtooth.transactions.accounting.AccountingHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Credit extends Entity implements EntityI{

	private final static String OBJ_IDENTIFIER = "1";

	private String creditNo;

	private String originatorAddress;

	private String beneficiaryAddress;

	private BigDecimal amount;

	private List<String> feeAddresses;

	public Credit(){
		super(AccountingHandler.FAMILY_NAME, OBJ_IDENTIFIER);
	}

	public Credit(String creditNo, String originatorAddress, String beneficiaryAddress, BigDecimal amount, List<String> feeAddresses) {
		this();
		this.creditNo = creditNo;
		this.originatorAddress = originatorAddress;
		this.beneficiaryAddress = beneficiaryAddress;
		this.amount = amount;
		this.feeAddresses = feeAddresses;
	}

	public Credit(String payload){
		this();
		final Credit credit = deserializePayload(payload);
		this.creditNo = credit.creditNo;
		this.originatorAddress = credit.originatorAddress;
		this.beneficiaryAddress = credit.beneficiaryAddress;
		this.amount = credit.amount;
		this.feeAddresses = credit.feeAddresses;
	}

	public String getCreditNo() {
		return creditNo;
	}

	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}

	public String getOriginatorAddress() {
		return originatorAddress;
	}

	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	public String getBeneficiaryAddress() {
		return beneficiaryAddress;
	}

	public void setBeneficiaryAddress(String beneficiaryAddress) {
		this.beneficiaryAddress = beneficiaryAddress;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public List<String> getFeeAddresses() {
		return feeAddresses;
	}

	public void setFeeAddresses(List<String> feeAddresses) {
		this.feeAddresses = feeAddresses;
	}

	@Override
	public List<AddressField> sortedAdressFields() {
		final List<AddressField> fields = new ArrayList<>();
		fields.add(new AddressField(0, 21, () -> creditNo, true));
		fields.add(new AddressField(0, 21, () -> beneficiaryAddress.substring(40, 70), false));
		fields.add(new AddressField(0, 21, () -> originatorAddress.substring(40, 70), false));
		return fields;
	}

	@Override
	public String toString() {
		return "Credit{" +
				"creditNo='" + creditNo + '\'' +
				", originatorAddress='" + originatorAddress + '\'' +
				", beneficiaryAddress='" + beneficiaryAddress + '\'' +
				", amount=" + amount +
				", feeAddresses=" + feeAddresses +
				'}';
	}

	@Override
	public Credit deserializePayload(String payload) {
		final ByteString byteString = ByteString.copyFromUtf8(payload);
		try {
			final EntityProtos.Credit protoCredit = EntityProtos.Credit.parseFrom(byteString);

			final Credit credit = new Credit(protoCredit.getCreditNo(), protoCredit.getOriginatorAddress(), protoCredit.getBeneficiaryAddress(),
				new BigDecimal(protoCredit.getAmount()), protoCredit.getFeeAddressesList());
			return credit;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String serializePayload() {
		final EntityProtos.Credit.Builder builder = EntityProtos.Credit.newBuilder()
			.setCreditNo(this.creditNo)
			.setOriginatorAddress(this.originatorAddress)
			.setBeneficiaryAddress(this.getBeneficiaryAddress())
			.setAmount(this.amount.toString())
			.addAllFeeAddresses(this.feeAddresses);

		final EntityProtos.Credit protoCredit = builder.build();
		return serializeMessage(protoCredit);
	}


}
