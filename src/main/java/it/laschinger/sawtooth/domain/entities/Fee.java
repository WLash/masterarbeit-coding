package it.laschinger.sawtooth.domain.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import it.laschinger.sawtooth.domain.entities.protobuf.entities.EntityProtos;
import it.laschinger.sawtooth.transactions.usage.UsageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Fee extends Entity implements EntityI {

	private static final Logger logger = LoggerFactory.getLogger(Fee.class);
	public final static String OBJ_IDENTIFIER = "1";

	@JsonProperty
	private String usageAddress = null;

	@JsonProperty
	private String beneficiaryAddress = null;

	@JsonProperty
	private String invoiceAddress = null;

	@JsonProperty
	private String creditAddress = null;

	@JsonProperty
	private BigDecimal amount = null;

	@JsonProperty
	private State state = null;


	public Fee(){
		super(UsageHandler.FAMILY_NAME, OBJ_IDENTIFIER);
	}

	public Fee(final String usageAddress, final String beneficiaryAddress, final String invoiceAddress, final String creditAddress, final BigDecimal amount, final State state){
		this();
		this.usageAddress = usageAddress;
		this.beneficiaryAddress = beneficiaryAddress;
		this.invoiceAddress = invoiceAddress;
		this.creditAddress = creditAddress;
		this.amount = amount;
		this.state = state;
	}

	public Fee(final String payload){
		this();
		final Fee fee = deserializePayload(payload);
		this.usageAddress = fee.getUsageAddress();
		this.beneficiaryAddress = fee.getBeneficiaryAddress();
		this.invoiceAddress = fee.getInvoiceAddress();
		this.creditAddress = fee.getCreditAddress();
		this.amount = fee.getAmount();
		this.state = fee.getState();
	}


	@Override
	public List<AddressField> sortedAdressFields() {
		final List<AddressField> fields = new ArrayList<>();
		fields.add(new AddressField(
			0, 4,
			() -> usageAddress.substring(7, 11),
			false)); //Usage Type
		fields.add(new AddressField(
			0, 11,
			() -> usageAddress.substring(11, 25),
			false)); //Distributor PubKey
		fields.add(new AddressField(
			0, 12,
			() -> usageAddress.substring(25, 40),
			false)); //Royalty Org Pub Key
		fields.add(
			new AddressField(
				0, 12,
				() -> usageAddress.substring(40, 55),
				false)); //Creation
		fields.add(new AddressField(
			0, 12,
			() -> usageAddress.substring(55, 70),
			false)); //Time
		fields.add(new AddressField(
			0, 12,
			() -> beneficiaryAddress.substring(40, 70),
			true)); //Beneficiary PubKey
		return fields;
	}

	public enum State{
		OPEN, INVOICED, PAYED, CREDITED;
	}

	public String getUsageAddress() {
		return usageAddress;
	}

	public void setUsageAddress(String usageAddress) {
		this.usageAddress = usageAddress;
	}

	public String getBeneficiaryAddress() {
		return beneficiaryAddress;
	}

	public void setBeneficiaryAddress(String beneficiaryAddress) {
		this.beneficiaryAddress = beneficiaryAddress;
	}

	public String getInvoiceAddress() {
		return invoiceAddress;
	}

	public void setInvoiceAddress(String invoiceAddress) {
		this.invoiceAddress = invoiceAddress;
	}

	public String getCreditAddress() {
		return creditAddress;
	}

	public void setCreditAddress(String creditAddress) {
		this.creditAddress = creditAddress;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Fee{" +
				"usageAddress='" + usageAddress + '\'' +
				", beneficiaryAddress='" + beneficiaryAddress + '\'' +
				", invoiceAddress='" + invoiceAddress + '\'' +
				", creditAddress='" + creditAddress + '\'' +
				", amount=" + amount +
				", state=" + state +
				'}';
	}

	@Override
	public Fee deserializePayload(String payload) {
		final ByteString byteString = ByteString.copyFromUtf8(payload);
		try {
			final EntityProtos.Fee protoFee = EntityProtos.Fee.parseFrom(byteString);

			final Fee fee = new Fee(protoFee.getUsageAddress(), protoFee.getBeneficiaryAddress(), protoFee.getInvoiceAddress(), protoFee.getCreditAddress(),
					new BigDecimal(protoFee.getAmount()), State.valueOf(protoFee.getState().name()));
			return fee;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String serializePayload() {
		final EntityProtos.Fee.Builder builder = EntityProtos.Fee.newBuilder()
				.setUsageAddress(this.usageAddress).setBeneficiaryAddress(this.beneficiaryAddress)
				.setAmount(this.amount.toString()).setState(EntityProtos.Fee.State.valueOf(this.state.name()));

		if(this.creditAddress != null) builder.setCreditAddress(this.creditAddress);
		if(this.invoiceAddress != null) builder.setInvoiceAddress(this.invoiceAddress);

		final EntityProtos.Fee protoFee = builder.build();
		return serializeMessage(protoFee);
	}
}
