package it.laschinger.sawtooth.domain.entities;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import it.laschinger.sawtooth.domain.entities.protobuf.entities.EntityProtos;
import it.laschinger.sawtooth.transactions.usage.UsageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Usage extends Entity implements EntityI {

	private final static String OBJ_IDENTIFIER = "0";
	private final static Logger logger = LoggerFactory.getLogger(Usage.class);

	private String tariffAddress;

	private String creationId;

	private Long time;

	private String text;

	public Usage(){
		super(UsageHandler.FAMILY_NAME, OBJ_IDENTIFIER);
	}

	public Usage(final String tariffAddress, final String creationId, final Long time, final String text){
		this();
		this.tariffAddress = tariffAddress;
		this.creationId = creationId;
		this.time = time;
		this.text = text;
	}

	public Usage(final String payload){
		this();
		final Usage usage = deserializePayload(payload);
		this.tariffAddress = usage.tariffAddress;
		this.creationId = usage.creationId;
		this.time = usage.time;
		this.text = usage.text;
	}


	@Override
	public List<AddressField> sortedAdressFields() {
		final List<AddressField> fields = new ArrayList<>();
		fields.add(new AddressField(0, 4, () -> tariffAddress.substring(7, 11), false)); //UsageType
		fields.add(new AddressField(0, 14, () -> tariffAddress.substring(11, 40), false)); //DISTRIBUTOR
		fields.add(new AddressField(0, 15, () -> tariffAddress.substring(40, 70), false)); //RO
		fields.add(new AddressField(0, 15, () -> creationId, true));
		fields.add(new AddressField(0, 15, () -> time.toString(), true));

		return fields;
	}

	public enum Type{
		DOWNLOAD, STREAMING;
	}

	public String getTariffAddress() {
		return tariffAddress;
	}

	public void setTariffAddress(String tariffAddress) {
		this.tariffAddress = tariffAddress;
	}

	public String getCreationId() {
		return creationId;
	}

	public void setCreationId(String creationId) {
		this.creationId = creationId;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Usage{" +
				"tariffAddress='" + tariffAddress + '\'' +
				", creationId=" + creationId +
				", time=" + time +
				", text='" + text + '\'' +
				'}';
	}

	@Override
	public Usage deserializePayload(String payload) {
		final ByteString byteString = ByteString.copyFromUtf8(payload);
		try {
			logger.debug("ByteString: " +  payload);
			final EntityProtos.Usage protoUsage = EntityProtos.Usage.parseFrom(byteString);
			logger.debug("ProtoUser: " + protoUsage);

			final Usage usage = new Usage(protoUsage.getTariffAddress(), protoUsage.getCreationId(), new Long(protoUsage.getTime()), protoUsage.getText());
			return usage;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String serializePayload() {
		logger.debug("This: " +  this);
		final EntityProtos.Usage.Builder builder = EntityProtos.Usage.newBuilder()
			.setTariffAddress(this.tariffAddress).setCreationId(this.creationId)
//			.setTime(this.time.toString())
			.setText(this.text);

		final EntityProtos.Usage protoUser = builder.build();
		return serializeMessage(protoUser);
	}
}
