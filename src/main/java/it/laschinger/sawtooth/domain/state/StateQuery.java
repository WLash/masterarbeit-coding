package it.laschinger.sawtooth.domain.state;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(value = { "paging" })
public class StateQuery {

	@JsonProperty("data")
	private List<StateQueryEntry> entries = new ArrayList<>();

	@JsonProperty
	private String head;

	@JsonProperty
	private String link;



	public List<StateQueryEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<StateQueryEntry> entries) {
		this.entries = entries;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}


	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public static StateQuery emptyStateQuery(){
		final StateQuery query = new StateQuery();
		query.setEntries(new ArrayList<>());
		return query;
	}

	@Override
	public String toString() {
		return "StateQuery{" +
				"entries=" + entries +
				", head='" + head + '\'' +
				", link='" + link + '\'' +
				'}';
	}
}
