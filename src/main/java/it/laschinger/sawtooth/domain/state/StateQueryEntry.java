package it.laschinger.sawtooth.domain.state;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StateQueryEntry {

	@JsonProperty
	private String address;

	@JsonProperty
	private String data;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "StateQueryEntry{" +
				"address='" + address + '\'' +
				", data='" + data + '\'' +
				'}';
	}
}
