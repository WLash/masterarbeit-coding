package it.laschinger.sawtooth.domain.state;

import com.fasterxml.jackson.annotation.JsonProperty;


public class AddressState {

	@JsonProperty
	private String data;

	@JsonProperty
	private String head;

	@JsonProperty
	private String link;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
