package it.laschinger.sawtooth.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import it.laschinger.sawtooth.domain.entities.Entity;
import it.laschinger.sawtooth.domain.entities.EntityI;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.domain.state.AddressState;
import it.laschinger.sawtooth.domain.state.StateQuery;
import it.laschinger.sawtooth.domain.state.StateQueryEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class StateUtils {

	private final static Logger logger = LoggerFactory.getLogger(StateUtils.class);

	public static String getStateForAddress(final State stateStore, final String address) throws InvalidTransactionException, InternalError {
		return stateStore.getState(Collections.singletonList(address)).get(address).toStringUtf8();
	}

	public static AddressState getStateForAddressRest(final RestApi api, final String address){
		final String json = api.sendGetRequest("state", address);
		final ObjectMapper mapper = new ObjectMapper();
		final AddressState addressState;
		try {
			addressState = mapper.readValue(json, AddressState.class);

			addressState.setData(GlobalUtils.decodeBase64String(addressState.getData()));
			return addressState;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static<T extends Entity & EntityI> boolean isEntityExisting(final RestApi api, final T entity) throws InvalidTransactionException {
//		final StateQuery query = queryState(api, AddressUtils.generateAddress(entity));
//		return query != null && !query.getEntries().isEmpty();
		return getStateForAddressRest(api, AddressUtils.generateAddress(entity)) != null;
	}

	/**
	 * Helper function to encode the State that will be stored at the address of the
	 * name.
	 *
	 * The implementation doestn do any encoding and add teh data with java
	 * Serialized bytes.
	 */
	public static Map.Entry<String, ByteString> encodeState(String address, String data) {

		return new AbstractMap.SimpleEntry<String, ByteString>(address, ByteString.copyFrom(data.getBytes()));
	}

	/**
	 * Helper function to decode State retrieved from the address of the name.
	 */
	public static String decodeState(byte[] bytes) {
		return new String(bytes);
	}

	public static Collection<String> setState(State stateStore, String address, String payload) throws InternalError, InvalidTransactionException {
		final Map.Entry<String, ByteString> entry = StateUtils.encodeState(address, payload);
		Collection<Map.Entry<String, ByteString>> adressValues = Collections.singletonList(entry);
		final Collection<String> addresses = stateStore.setState(adressValues);
		if(addresses.size() == 0){
			throw new java.lang.InternalError("State error! Data size is zero");
		}
		return addresses;
	}

	public static StateQuery queryState (final RestApi api, final String addressPiece){
		final String url = api.getRestUrl("state?address="+addressPiece);
		logger.debug("URL: " + url);
		final String jsonString = api.sendGetRequest("state?address="+addressPiece);
		if(jsonString.isEmpty()) return null;
		final ObjectMapper mapper = new ObjectMapper();
		try {
			final StateQuery query = mapper.readValue(jsonString, StateQuery.class);
			query.getEntries().forEach(e -> e.setData(GlobalUtils.decodeBase64String(e.getData())));
			return query;
		} catch (IOException e) {
			return StateQuery.emptyStateQuery();
		}
	}

	public static User queryUser(final RestApi api, final User user) throws IOException {
		final String address = AddressUtils.generateAddress(user);
		final AddressState state = StateUtils.getStateForAddressRest(api, address);
		if(state != null && !state.getData().isEmpty()){
			return new User(state.getData());
		}
		return null;
	}

	public static List<StateQueryEntry> queryAddressPiece(final RestApi api, final String addressPiece){
		final String namespace = addressPiece.substring(0, 6);
		final StateQuery query = queryState(api, namespace);
		final List<StateQueryEntry> entries = query == null ? StateQuery.emptyStateQuery().getEntries() : query.getEntries();
		return entries.stream().filter(e -> e.getAddress().startsWith(addressPiece)).collect(Collectors.toList());
	}

}
