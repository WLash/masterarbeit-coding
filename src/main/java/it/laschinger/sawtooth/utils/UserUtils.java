package it.laschinger.sawtooth.utils;

import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.domain.state.AddressState;
import it.laschinger.sawtooth.domain.state.StateQuery;
import it.laschinger.sawtooth.domain.state.StateQueryEntry;
import it.laschinger.sawtooth.transactions.user.UserHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserUtils {

	private final static Logger logger = LoggerFactory.getLogger(UserUtils.class);

	public static boolean isFirstUser(final RestApi api){
		final StateQuery query;
		query = StateUtils.queryState(api, GlobalUtils.getDefaultNameSpace(UserHandler.FAMILY_NAME));
		if(query == null) return false;
		logger.info("StateQuery: " + query);
		return query.getEntries().isEmpty();
	}

	public static User getUserByUser(final RestApi api, final User user) {
		final AddressState state = StateUtils.getStateForAddressRest(api, AddressUtils.generateAddress(user));
		if(state == null){ return null; }
		final String data = state.getData();
		return new User(data);
	}

	public static List<User> getAllArtists(final RestApi api) {
		return getAllUsersByRole(api, User.Role.ARTIST);
	}

	private static List<User> getAllUsersByRole(RestApi api, User.Role role){
		final String hash = GlobalUtils.hashString(role.name());
		if(hash == null) return new ArrayList<>();
		final String rolePiece = hash.substring(0, 4);

		return getUsersByAddressPiece(api, rolePiece);
	}

	public static User getUserByPublicKey(final RestApi api, final String publicKey) throws InvalidTransactionException {
		final String hash = GlobalUtils.hashString(publicKey);
		final String keyPiece = hash.substring(0, 30);

		final List<StateQueryEntry> entries = getAllUserEntries(api);
		if(entries.isEmpty()) return null;

		final List<User> users =
			entries.stream().filter(e -> {
				final String addressPiece = e.getAddress().substring(40, 70);
				final boolean test = addressPiece.equals(keyPiece);
				return test;
			})
				.map(e -> new User(e.getData()))
				.collect(Collectors.toList());
		if(users.size() > 1) throw new InvalidTransactionException("More than one User found: " + users);
		return users.isEmpty() ? null : users.get(0);
	}

	public static User getUserByCreationDbId(final RestApi api, final String creationDbId) throws InvalidTransactionException {
		final String hash = GlobalUtils.hashString(creationDbId);
		final String keyPiece = hash.substring(0, 29);

		final List<StateQueryEntry> entries = getAllUserEntries(api);
		if(entries.isEmpty()) return null;

		final List<User> users =
				entries.stream().filter(e -> {
					final String addressPiece = e.getAddress().substring(11, 40);
					final boolean test = addressPiece.equals(keyPiece);
					return test;
				})
						.map(e -> new User(e.getData()))
						.collect(Collectors.toList());
		if(users.size() > 1) throw new InvalidTransactionException("More than one User found");
		return users.isEmpty() ? null : users.get(0);
	}

	public static List<User> getAllPublisher(final RestApi api){
		return getAllUsersByRole(api, User.Role.PUBLISHER);
	}

	public static boolean hasUserCreationDbId(final String pubKey, final List<User> users, final String creationDbId){
		return users.stream().anyMatch(u -> u.getCreationDbId().equals(creationDbId) && !u.getPublicKeyHex().equals(pubKey));
	}

	public static String getUserNamespace(){
		return GlobalUtils.hashString(UserHandler.FAMILY_NAME).substring(0,6) + User.OBJ_IDENTIFIER;
	}

	public static List<User> getAllUsers(final RestApi api){
		return mapEntriesToUsers(getAllUserEntries(api));
	}

	public static List<User> getUsersByAddressPiece(final RestApi api, final String addressPiece) {
		final StringBuilder sb = new StringBuilder(getUserNamespace());
		if(addressPiece != null){
			sb.append(addressPiece);
		}

		return mapEntriesToUsers(StateUtils.queryAddressPiece(api, sb.toString()));

	}

	public static List<User> mapEntriesToUsers(final List<StateQueryEntry> entries){
		return entries.stream().map(e -> new User(e.getData())).collect(Collectors.toList());
	}

	public static List<StateQueryEntry> getAllUserEntries(final RestApi api){
		return StateUtils.queryAddressPiece(api, getUserNamespace());
	}

	public static User getUserByAddress(RestApi api, String address) {
//		final StateQuery query = StateUtils.queryState(api, address);
//		if(query.getEntries().size() > 1) throw new InvalidTransactionException("More than one entry found");
//		if(query.getEntries().isEmpty()) return null;
//		return new User(query.getEntries().get(0).getData());
		final AddressState state = StateUtils.getStateForAddressRest(api, address);
		return state == null ? null : new User(state.getData());
	}
}
