package it.laschinger.sawtooth.utils;

import it.laschinger.sawtooth.domain.entities.AddressField;
import it.laschinger.sawtooth.domain.entities.Entity;
import it.laschinger.sawtooth.domain.entities.EntityI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AddressUtils {

	private final static Logger logger = LoggerFactory.getLogger(AddressUtils.class);

	public static <T extends Entity & EntityI> String generateAddress(
			final T entity){
		return generateAddress(entity.getFamilyName(),
			entity.getObjIdentifier(),
			entity.sortedAdressFields());
	}

	public static String generateAddress(final String familyName,
			final String objIdentifier,
		 	final List<AddressField> fields){
		final StringBuilder sb = new StringBuilder();
		sb.append(GlobalUtils.hashString(familyName), 0, 6)
			.append(objIdentifier);
		fields.forEach(f -> {
			sb.append(f.getValue(), f.getStart(), f.getEnd());
		});
		return sb.toString();
	}

}
