package it.laschinger.sawtooth.utils;

public class Actions {

	public static final String ADD_ACTION = "add";
	public static final String INIT_ACTION = "init";
	public static final String CONFIRM_ACTION = "confirm";
	public static final String DECLINE_ACTION = "decline";
}
