package it.laschinger.sawtooth.utils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.NoHttpResponseException;
import org.awaitility.Awaitility;
import org.awaitility.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class RestApi {

	private final static Logger logger = LoggerFactory.getLogger(RestApi.class);

	public static String ACCEPT_HEADER_NAME = "accept";
	public static String JSON_HEADER_VALUE = "application/json";
	private static String CONTENT_TYPE_HEADER_NAME = "Content-Type";
	private static String OCTET_HEADER_VALUE = "application/octet-stream";

	private final String url;

	public RestApi(final String serverName, final String port, final String path){
		final StringBuilder sb = new StringBuilder("http://");
		sb.append(serverName)
			.append(port != null ? ":"+port : "")
			.append(path != null ? "/" + path : "");
		url = sb.toString();
	}

	public String getRestUrl(final String... pathVariables){
		final StringBuilder sb = new StringBuilder(url);

		Arrays.stream(pathVariables).forEach(s-> sb.append("/").append(s));
		return sb.toString();
	}

	public HttpResponse<JsonNode> getJsonResponse(final String... pathVariables) throws UnirestException {
		final String url = getRestUrl(pathVariables);
		logger.debug("Create GET-Request for URL: " + url);
		return Unirest.get(url).header(ACCEPT_HEADER_NAME, JSON_HEADER_VALUE).asJson();
	}

	public HttpResponse postResponse(final byte[] bodyBytes, final String... pathVariables) throws UnirestException, NoHttpResponseException{
		final String url = getRestUrl(pathVariables);
		logger.debug("Create POST-Request for URL: " + url);
		return Unirest.post(url).header(CONTENT_TYPE_HEADER_NAME, OCTET_HEADER_VALUE).body(bodyBytes).asString();
	}

	public HttpResponse<JsonNode> jsonPostResponse(final String json, final String... pathVariables) throws UnirestException {
		final String url = getRestUrl(pathVariables);
		final JsonNode jsonNode = new JsonNode(json);
		return Unirest.post(url)
			.header(ACCEPT_HEADER_NAME, JSON_HEADER_VALUE)
			.header(CONTENT_TYPE_HEADER_NAME, JSON_HEADER_VALUE)
			.body(jsonNode).asJson();
	}


	public String sendPostRequest(final byte[] bodyBytes, final String... pathVariables)  {
		final StringBuilder sb = new StringBuilder();
		final AtomicInteger counter = new AtomicInteger(0);
		Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted(() -> {
			try {
				logger.info("Try: " + counter.incrementAndGet());
				HttpResponse response = postResponse(bodyBytes, pathVariables);
				Thread.sleep(Duration.ONE_SECOND.getValueInMS());
				logger.info("POST-Request-Status: " + response.getStatus());
				logger.info("POST-Request-Body: " + response.getBody());
				assert response.getStatus() == 202 || response.getStatus() == 429 : "Invalid Response Status: " + response.getStatus();
				sb.append(response.getBody().toString());
			}catch (Exception e){
				e.printStackTrace();
				Thread.sleep(3000);
				assert false;
			}
		});
		assert !sb.toString().isEmpty();
		return sb.toString();
	}

	public HttpResponse<JsonNode> sendJsonPostRequest(final String json, final String... pathVariables){
		final List<HttpResponse<JsonNode>> singletonList = new ArrayList<>();
		Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted(() -> {
			singletonList.add(0, jsonPostResponse(json, pathVariables));
		});
		return singletonList.get(0);
	}

	/**
	 *
	 * @param pathVariables
	 * @return JsonResponse of GET-Request
	 */
	public String sendGetRequest(final String... pathVariables){
		logger.debug("sendGetRequest");
		final StringBuilder sb = new StringBuilder();

		final AtomicBoolean firstRun = new AtomicBoolean(false);
		try {
			logger.debug("sendRequestAwaitStart");
			final HttpResponse<JsonNode> response = getJsonResponse(pathVariables);
			logger.debug("Response-Status: " + response.getStatus());
			final String json = response.getBody().toString();
			sb.append(json);
		} catch (UnirestException e) {
			logger.debug("No Json-Response");
		}
		return sb.toString();
	}

	public String getUrl() {
		return url;
	}
}
