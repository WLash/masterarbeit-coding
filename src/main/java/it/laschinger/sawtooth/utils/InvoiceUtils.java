package it.laschinger.sawtooth.utils;

import it.laschinger.sawtooth.domain.entities.Invoice;
import it.laschinger.sawtooth.domain.state.StateQueryEntry;
import it.laschinger.sawtooth.transactions.accounting.AccountingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

import java.util.List;
import java.util.stream.Collectors;

public class InvoiceUtils {
	private final static Logger logger = LoggerFactory.getLogger(InvoiceUtils.class);

	public static Invoice getInvoiceByNoAndOriginator(final RestApi api, final String invoiceNo, final String publicKey) throws InvalidTransactionException {

		final List<StateQueryEntry> entries = getAllInvoiceEntries(api);
		final List<Invoice> invoices = entries.stream().filter(e -> {
			logger.info("InvoiceAddress: " + e.getAddress());
			final String invoicePart = e.getAddress().substring(7, 28);
			final String searchForInvoice = GlobalUtils.hashString(invoiceNo).substring(0,21);

			final String originatorPart = e.getAddress().substring(49, 70);
			final String searchForOriginator = GlobalUtils.hashString(publicKey).substring(0, 21);

			logger.info("invoicePart: " + invoicePart);
			logger.info("searchForInvoice: " + searchForInvoice);
			logger.info("OriginatorPart: " + originatorPart);
			logger.info("searchForOriginator: " + searchForOriginator);

			return invoicePart.equals(searchForInvoice) &&
				originatorPart.equals(searchForOriginator);
		}).map(e -> new Invoice(e.getData())).collect(Collectors.toList());

		if(invoices.isEmpty()) return null;
		if(invoices.size() > 1) throw new InvalidTransactionException("More than one invoice found");
		return invoices.get(0);
	}

	public static List<StateQueryEntry> getAllInvoiceEntries(final RestApi api){
		return StateUtils.queryAddressPiece(api, getInvoiceNamespace());
	}

	public static String getInvoiceNamespace(){
		return new AccountingHandler().getNameSpace() + Invoice.OBJ_IDENTIFIER;
	}
}
