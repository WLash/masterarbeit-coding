package it.laschinger.sawtooth.utils;

import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.domain.state.AddressState;
import it.laschinger.sawtooth.domain.state.StateQueryEntry;
import it.laschinger.sawtooth.transactions.tariff.TariffHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

import java.util.List;
import java.util.stream.Collectors;

public class TariffUtils {

	private final static Logger logger = LoggerFactory.getLogger(TariffUtils.class);

	public static Tariff getTariffByAddress(final RestApi api, final String address) throws InvalidTransactionException {
//		final StateQuery query = StateUtils.queryState(api, address);
//		if(query.getEntries().size() > 1) throw new InvalidTransactionException("More than one entry found");
//		if(query.getEntries().isEmpty()) return null;
//		return new Tariff(query.getEntries().get(0).getData());
		final AddressState state = StateUtils.getStateForAddressRest(api, address);
		return state == null ? null : new Tariff(state.getData());

	}

	public static Tariff getTariffByUsers(RestApi api, User ro, User distributor, final Usage.Type usageType) throws InvalidTransactionException {
		logger.info("GetTariffByUsers:");
		final String roAddress = AddressUtils.generateAddress(ro);
		final String distributorAddress = AddressUtils.generateAddress(distributor);
		final String hashUsageType = GlobalUtils.hashString(usageType.name());

		final List<StateQueryEntry> entries = getAllTariffEntries(api);

		final List<Tariff> tariffs = getAllTariffEntries(api).stream().filter(e ->{
			final String address = e.getAddress();
//			logger.info("Address: " + address);
			final String tariffUsageType = address.substring(7, 11);
			final String lfHashUsageType = hashUsageType.substring(0, 4);
//			logger.info("UsageType-Compare: " + tariffUsageType + " " + lfHashUsageType);
			final String tariffDistPub = address.substring(11, 40);
			final String lfDistPub = distributorAddress.substring(40, 69);
//			logger.info("DistPub-Compare: " + tariffDistPub + " " + lfDistPub);
			final String tariffRoPub = address.substring(40, 70);
			final String lfRoPub = roAddress.substring(40, 70);
//			logger.info("RoPub-Compare: " + tariffRoPub + " " + lfRoPub);

			return tariffUsageType.equals(lfHashUsageType) &&
				tariffDistPub.equals(lfDistPub) &&
				tariffRoPub.equals(lfRoPub);

		}).map(e -> new Tariff(e.getData())).collect(Collectors.toList());
		if(tariffs.isEmpty()) return null;
		if(tariffs.size()>1) throw new InvalidTransactionException("Too much Tariffs found");
		return tariffs.get(0);

	}

	public static List<StateQueryEntry> getAllTariffEntries(final RestApi api){
		return StateUtils.queryAddressPiece(api, getTariffNamespace());
	}

	public static String getTariffNamespace(){
		return GlobalUtils.hashString(TariffHandler.FAMILY_NAME).substring(0,6) + User.OBJ_IDENTIFIER;

	}
}
