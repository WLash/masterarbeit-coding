package it.laschinger.sawtooth.utils;

import org.awaitility.Awaitility;
import org.awaitility.Duration;
import org.awaitility.core.ConditionTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

public class AwaitilityUtils {

	private final static Logger logger = LoggerFactory.getLogger(AwaitilityUtils.class);

	public static boolean testAwaitilityUntilTrue(final Supplier<Boolean> supplier, Duration duration) {
		try {
			final AtomicBoolean bool = new AtomicBoolean(false);
			final AtomicBoolean isFirstRun = new AtomicBoolean(true);
			Awaitility.await().atMost(duration).until(() -> {
				if(!isFirstRun.get()) Thread.sleep(500);
				final boolean isSuccessful = supplier.get();
				bool.set(isSuccessful);
				isFirstRun.set(false);
				logger.info("Awaitility: " + bool.get());
				return bool.get();
			});
			return bool.get();
		}catch (ConditionTimeoutException e){
			return false;
		}
	}
}
