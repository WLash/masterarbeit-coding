package it.laschinger.sawtooth.utils;

import com.google.protobuf.ByteString;
import it.laschinger.sawtooth.transactions.accounting.AccountingHandler;
import it.laschinger.sawtooth.transactions.tariff.TariffHandler;
import it.laschinger.sawtooth.transactions.usage.UsageHandler;
import it.laschinger.sawtooth.transactions.user.UserHandler;
import org.bitcoinj.core.ECKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.client.Signing;
import sawtooth.sdk.processor.Utils;
import sawtooth.sdk.protobuf.Batch;
import sawtooth.sdk.protobuf.BatchHeader;
import sawtooth.sdk.protobuf.Transaction;
import sawtooth.sdk.protobuf.TransactionHeader;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TransactionUtils {

	final private static Logger logger = LoggerFactory.getLogger(TransactionUtils.class);

	public static Transaction getTxnUser(final ECKey privateKey, final String payload){
		final TransactionHeader txnHeader = getTxnHeaderUser(privateKey.getPublicKeyAsHex(), payload, null);
		return getTxn(privateKey, txnHeader, payload);
	}

	public static Transaction getTxnUserWithDependencies(final ECKey privateKey, final String payload, final List<String> dependencies){
		final TransactionHeader txnHeader = getTxnHeaderUser(privateKey.getPublicKeyAsHex(), payload, dependencies);
		return getTxn(privateKey, txnHeader, payload);
	}

	public static Transaction getTxnTariff(final ECKey privateKey, final String payload, final List<String> dependencies){
		final TransactionHeader txnHeader = getTxnHeaderTariff(privateKey.getPublicKeyAsHex(), payload, dependencies);
		return getTxn(privateKey, txnHeader, payload);
	}

	public static Transaction getTxnUsage(final ECKey key, final String payload, final List<String> dependencies){
		final TransactionHeader txnHeader = getTxnHeaderUsage(key.getPublicKeyAsHex(), payload, dependencies);
		return getTxn(key, txnHeader, payload);
	}

	public static Transaction getTxnAccounting(final ECKey key, final String payload){
		final TransactionHeader txnHeader = getTxnHeaderAccounting(key.getPublicKeyAsHex(), payload);
		return getTxn(key, txnHeader, payload);
	}

	public static Transaction getTxn(final ECKey key,
									 final TransactionHeader txnHeader,
									 final String payload){
		final ByteString txnHeaderBytes = txnHeader.toByteString();
		final ByteString payloadByteString = ByteString.copyFrom(payload.getBytes());
		final String value = Signing.sign(key, txnHeader.toByteArray());
		final Transaction txn = Transaction.newBuilder()
				.setHeader(txnHeaderBytes).setPayload(payloadByteString)
				.setHeaderSignature(value).build();
		return txn;
	}

	public static TransactionHeader getTxnHeaderUser(final String publicKey,
												 	 final String payload,
													 final List<String> dependencies){
		final List<String> inAndOutputs = new ArrayList<>();
		inAndOutputs.add(Objects.requireNonNull(
			GlobalUtils.hashString(UserHandler.FAMILY_NAME)).substring(0,6));
		return getTxnHeader(
			UserHandler.FAMILY_NAME,
			UserHandler.VERSION,
			publicKey,
			payload,
			inAndOutputs,
			inAndOutputs,
			dependencies);
	}


	public static TransactionHeader getTxnHeaderTariff(final String publicKey, final String payload, final List<String> dependencies){
		final List<String> inAndOutputs = new ArrayList<>();
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(UserHandler.FAMILY_NAME)).substring(0,6));
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(TariffHandler.FAMILY_NAME).substring(0,6)));
		return getTxnHeader(TariffHandler.FAMILY_NAME, TariffHandler.VERSION, publicKey, payload, inAndOutputs, inAndOutputs, dependencies);
	}

	public static TransactionHeader getTxnHeaderUsage(final String publicKey, final String payload, final List<String> dependencies){
		final List<String> inAndOutputs = new ArrayList<>();
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(UserHandler.FAMILY_NAME)).substring(0, 6));
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(UsageHandler.FAMILY_NAME)).substring(0, 6));
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(TariffHandler.FAMILY_NAME)).substring(0, 6));
		return getTxnHeader(UsageHandler.FAMILY_NAME, TariffHandler.VERSION, publicKey, payload, inAndOutputs, inAndOutputs, dependencies);
	}

	public static TransactionHeader getTxnHeaderAccounting(final String publicKey, final String payload){
		final List<String> inAndOutputs = new ArrayList<>();
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(UserHandler.FAMILY_NAME)).substring(0, 6));
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(AccountingHandler.FAMILY_NAME)).substring(0, 6));
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(UsageHandler.FAMILY_NAME)).substring(0, 6));
		inAndOutputs.add(Objects.requireNonNull(GlobalUtils.hashString(TariffHandler.FAMILY_NAME)).substring(0, 6));
		return getTxnHeader(AccountingHandler.FAMILY_NAME, AccountingHandler.VERSION, publicKey, payload, inAndOutputs, inAndOutputs, new ArrayList<>());
	}

	public static TransactionHeader getTxnHeader(final String txnFamilyName,
												 final String txnFamilyVer,
											 	 final String publicKey,
												 final String payload,
												 final List<String> inputs,
												 final List<String> outputs,
												 final List<String> dependencies){
		final String payloadBytes = Utils.hash512(payload.getBytes());
		final TransactionHeader.Builder builder = TransactionHeader
			.newBuilder().clearBatcherPublicKey()
				.setBatcherPublicKey(publicKey)
				.setFamilyName(txnFamilyName)
				.setFamilyVersion(txnFamilyVer)
				.addAllInputs(inputs)
				.setNonce("1")
				.addAllOutputs(outputs)
				.setPayloadSha512(payloadBytes)
				.setSignerPublicKey(publicKey);
		if(dependencies != null) builder.addAllDependencies(dependencies);

		return builder.build();
	}

	public static Batch createBatch(ECKey privateKey, List<Transaction> txns){
		final BatchHeader.Builder batchHeaderBuilder =
			BatchHeader.newBuilder()
				.clearSignerPublicKey()
				.setSignerPublicKey(privateKey.getPublicKeyAsHex());
		txns.forEach(
			t -> batchHeaderBuilder.addTransactionIds(t.getHeaderSignature()));

		final BatchHeader batchHeader = batchHeaderBuilder.build();
		final ByteString batchHeaderBytes = batchHeader.toByteString();

		final String value_batch = Signing.sign(privateKey, batchHeader.toByteArray());
		final Batch.Builder batchBuilder = Batch.newBuilder()
			.setHeader(batchHeaderBytes)
			.setHeaderSignature(value_batch)
			.setTrace(true);
		txns.forEach(batchBuilder::addTransactions);
		return batchBuilder.build();
	}


}
