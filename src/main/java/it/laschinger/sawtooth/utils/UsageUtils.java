package it.laschinger.sawtooth.utils;

import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.domain.state.AddressState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

public class UsageUtils {

	private final static Logger logger = LoggerFactory.getLogger(UsageUtils.class);

	public static Usage getUsageByAddress(final RestApi api, final String address) throws InvalidTransactionException {
//		final StateQuery query = StateUtils.queryState(api, address);
//		if(query.getEntries().size() > 1) throw new InvalidTransactionException("More than one entry found for Usage address");
//		if(query.getEntries().isEmpty()) return null;
//		logger.info("Data: " +  query.getEntries().get(0).getData());
//		final Usage usage = new Usage(query.getEntries().get(0).getData());
//		logger.info("Usage: " +  usage);
//		return usage;
		final AddressState state = StateUtils.getStateForAddressRest(api, address);
		return state == null ? null : new Usage(state.getData());
	}
}
