package it.laschinger.sawtooth.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.Utils;
import sawtooth.sdk.protobuf.TpProcessRequest;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Objects;

public class GlobalUtils {

	public static String UTF_8 = "UTF-8";
	private static Logger logger = LoggerFactory.getLogger(GlobalUtils.class);


	public static String hashString(String s) {
		try {
			return Utils.hash512(s.getBytes(UTF_8));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Implementers to provide logic for extracting data. Make is abstracts
	 *
	 * @param request
	 * @return
	 */
	public static String decodePayload(final TpProcessRequest request) {
		String payload = new String(request.getPayload().toByteArray());
		return payload;
	}


	public static String decodeBase64String(final String s){
		byte[] asBytes = Base64.getDecoder().decode(s);
		try {
			return new String(asBytes, UTF_8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getDefaultNameSpace(final String familyName){
		return Objects.requireNonNull(hashString(familyName)).substring(0, 6);
	}
}
