package it.laschinger.sawtooth.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.laschinger.sawtooth.domain.creation.Creation;
import it.laschinger.sawtooth.domain.creation.CreationDbEntity;
import it.laschinger.sawtooth.domain.creation.CreationShare;
import it.laschinger.sawtooth.domain.creation.CreationUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class CreationUtils {

	final static Logger logger = LoggerFactory.getLogger(CreationUtils.class);
	final static RestApi api = new RestApi("10.33.13.6", "8080", null);

	final static String USER_PATH_VAR = "user";
	final static String CREATION_PATH_VAR = "creation";
	final static String SHARE_PATH_VAR = "share";


	public static CreationUser getCreationUserById(final String id){
		return getCreationDbJsonResponse(CreationUser.class, id);
	}

	public static CreationShare getCreationShareById(final String id){
		return getCreationDbJsonResponse(CreationShare.class, id);
	}

	public static Creation getCreationById(final String id){
		final Creation creation = getCreationDbJsonResponse(Creation.class, id);
		return creation;
	}

	private static <T extends CreationDbEntity> T getCreationDbJsonResponse(Class<T> clazz, String id) {

		String pathVar;
		if(clazz.equals(CreationUser.class)){
			pathVar=USER_PATH_VAR;
		} else if (clazz.equals(CreationShare.class)){
			pathVar=SHARE_PATH_VAR;
		}else{
			pathVar=CREATION_PATH_VAR;
		}

		final String json = api.sendGetRequest(pathVar, String.valueOf(id));
		final ObjectMapper mapper = new ObjectMapper();
		try{
			final T obj = mapper.readValue(json, clazz);
			return obj;
		}catch (IOException e){
			e.printStackTrace();
			return null;
		}
	}






}
