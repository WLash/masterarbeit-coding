package it.laschinger.sawtooth.utils;

import it.laschinger.sawtooth.domain.entities.Fee;
import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.domain.state.StateQueryEntry;
import it.laschinger.sawtooth.transactions.usage.UsageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

import java.util.List;
import java.util.stream.Collectors;

public class FeeUtils {

	final private static Logger logger = LoggerFactory.getLogger(FeeUtils.class);

	public static List<Fee> getFeesByUsage(RestApi api, final Usage usage){
		final String usageAddress = AddressUtils.generateAddress(usage);
		final List<StateQueryEntry> entries = getAllFees(api);

		final List<Fee> fees = entries.stream().filter(e -> {
			final String address = e.getAddress();
			return address.substring(7, 11).equals(usageAddress.substring(7, 11)) &&
				address.substring(11, 22).equals(usageAddress.substring(11, 22)) &&
				address.substring(22, 34).equals(usageAddress.substring(25, 37)) &&
				address.substring(34, 46).equals(usageAddress.substring(40, 52)) &&
				address.substring(46, 58).equals(usageAddress.substring(55, 67));

		}).map(e -> new Fee(e.getData())).collect(Collectors.toList());

		return fees;
	}

	public static List<StateQueryEntry> getAllFees(final RestApi api){
		return StateUtils.queryAddressPiece(api, getFeeNamespace());
	}

	public static String getFeeNamespace(){
		return new UsageHandler().getNameSpace() + Fee.OBJ_IDENTIFIER;
	}

	public static Tariff getTariffOfFee(final RestApi api, final Fee fee) throws InvalidTransactionException {
		logger.debug("Usage-Address: " + fee.getUsageAddress());
		final Usage usage = UsageUtils.getUsageByAddress(api, fee.getUsageAddress());
		logger.debug("Usage: " + usage);
		logger.debug("Tariff-Address: " + usage.getTariffAddress());
		final Tariff tariff = TariffUtils.getTariffByAddress(api, usage.getTariffAddress());
		return tariff;
	}

	public static Fee getFeeFromState(final State state, final String address) throws InvalidTransactionException, InternalError {
		final String data = StateUtils.getStateForAddress(state, address);
		return new Fee(data);
	}
}
