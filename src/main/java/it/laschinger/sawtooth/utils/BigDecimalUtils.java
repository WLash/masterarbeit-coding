package it.laschinger.sawtooth.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUtils {

	public static BigDecimal striptTrailingZeros(final BigDecimal value){
		return value.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
	}

	public static BigDecimal calcQuotient(final BigDecimal value, final BigDecimal quotient){
		return value.divide(BigDecimal.valueOf(100)).multiply(quotient);
	}

	public static BigDecimal getRandomQuotient(){
		return getRandominRange(new BigDecimal(100.0));
	}

	public static BigDecimal getRandominRange(final BigDecimal range){
		BigDecimal randFromDouble = new BigDecimal(Math.random());
		return randFromDouble.divide(range,BigDecimal.ROUND_DOWN).setScale(0, RoundingMode.CEILING);
	}

	public static boolean equalValue(final BigDecimal value1, final BigDecimal value2){
		return value1.compareTo(value2) == 0;
	}

	public static boolean isValueLessZero(final BigDecimal value){
		return value.compareTo(BigDecimal.ZERO) <= 0;
	}
}
