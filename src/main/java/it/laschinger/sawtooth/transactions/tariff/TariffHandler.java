package it.laschinger.sawtooth.transactions.tariff;

import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.request.tariff.AddTariffReq;
import it.laschinger.sawtooth.transactions.AbstractHandler;
import it.laschinger.sawtooth.utils.Actions;
import it.laschinger.sawtooth.utils.AddressUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;

import java.io.IOException;

public class TariffHandler extends AbstractHandler {

	private final Logger logger = LoggerFactory.getLogger(TariffHandler.class);
	public final static String FAMILY_NAME = "tariff";
	public final static String VERSION = "1.0";


	public TariffHandler(){
		super(FAMILY_NAME, VERSION);
	}

	@Override
	public void apply(TpProcessRequest transactionRequest, State stateStore)
			throws InvalidTransactionException, InternalError {
		super.apply(transactionRequest, stateStore);
		final AddTariffReq req = parseRequest();

		final String state = getState(req.getDistributorAddress());
		final User distributor = new User(state);
		final User royaltyOrganisation =
			new User(getState(req.getRoyaltyOrganisationAddress()));

		logger.debug("Distributor: " + distributor);
		checkDistributor(distributor);
		checkRoyaltyOrganisation(royaltyOrganisation);
		checkValueBiggerZero(req.getAmount());

		final Tariff tariff =
			new Tariff(req.getDistributorAddress(),
					req.getRoyaltyOrganisationAddress(),
					req.getUsageType(),
					req.getAmount());
		final String tariffAddress = AddressUtils.generateAddress(tariff);

		setState(tariffAddress, tariff.serializePayload());

		logger.debug("Data has been written to " + tariffAddress + ". ");
	}

	private AddTariffReq parseRequest() throws InvalidTransactionException {
		try {
			final AddTariffReq req =
				getMapper().readValue(getRequestPayload(), AddTariffReq.class);
			if(!req.getAction().equals(Actions.ADD_ACTION)){
				throw new InvalidTransactionException("Invalid Action: " + req.getAction());
			}
			return req;
		} catch (IOException e) {
			throw new InvalidTransactionException("Request is no AddTariffRequest");
		}
	}
}
