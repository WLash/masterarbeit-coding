package it.laschinger.sawtooth.transactions.tariff;

import sawtooth.sdk.processor.TransactionProcessor;

public class TariffProcessor {

	public static void main(String[] args) {
		TransactionProcessor transactionProcessor = new TransactionProcessor(args[0]);
		transactionProcessor.addHandler(new TariffHandler());
		Thread thread = new Thread(transactionProcessor);
		thread.start();
	}
}
