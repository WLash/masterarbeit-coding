package it.laschinger.sawtooth.transactions.accounting;

import it.laschinger.sawtooth.domain.entities.*;
import it.laschinger.sawtooth.request.AbstractRequest;
import it.laschinger.sawtooth.request.accounting.AddCreditReq;
import it.laschinger.sawtooth.request.accounting.AddInvoiceReq;
import it.laschinger.sawtooth.request.accounting.AddPaymentReq;
import it.laschinger.sawtooth.transactions.AbstractHandler;
import it.laschinger.sawtooth.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;


public class AccountingHandler extends AbstractHandler {

	private final static Logger logger = LoggerFactory.getLogger(AccountingHandler.class);

	public final static String FAMILY_NAME = "accounting";
	public final static String VERSION = "1.0";

	public AccountingHandler() {
		super(FAMILY_NAME, VERSION);
	}

	@Override
	public void apply(TpProcessRequest transactionRequest, State state) throws InvalidTransactionException, InternalError {
		super.apply(transactionRequest, state);
		final AbstractRequest request = identifyRequest();
		if(request instanceof AddInvoiceReq){
			handleAddInvoice((AddInvoiceReq) request, transactionRequest);
		} else if (request instanceof AddPaymentReq){
			handleAddPayment((AddPaymentReq) request, transactionRequest);
		} else if(request instanceof AddCreditReq){
			handleAddCredit((AddCreditReq) request, transactionRequest);
		}
	}

	private void handleAddCredit(AddCreditReq request, TpProcessRequest transactionRequest) throws InvalidTransactionException, InternalError {
		if(!request.getAction().equals(Actions.ADD_ACTION)) throw new InvalidTransactionException("False Action for Credit-Request");
		final User originator = UserUtils.getUserByPublicKey(getApi(), transactionRequest.getHeader().getSignerPublicKey());
		if(originator == null) throw new InvalidTransactionException("Originator can't be found");
		checkRoyaltyOrganisation(originator);
		final User beneficiary = new User(getState(request.getBeneficiaryAddress()));

		final boolean isRoCredit = beneficiary.equals(originator);
		if(beneficiary.getRole()== User.Role.DISTRIBUTOR) throw new InvalidTransactionException("DISTRIBUTOR can't be Beneficiary");

		BigDecimal creditAmount = BigDecimal.ZERO;

		final Credit credit = new Credit(request.getCreditNo(), request.getOriginatorAddress(), request.getBeneficiaryAddress(), null, request.getFeeAddresses());
		for(String feeAddress : request.getFeeAddresses()){
			final Fee fee = new Fee(StateUtils.getStateForAddress(getStateStore(), feeAddress));

			if(fee.getState() != Fee.State.PAYED) throw new InvalidTransactionException("Only payed fees can be credited");
			if(!fee.getBeneficiaryAddress().equals(request.getBeneficiaryAddress())) throw new InvalidTransactionException("Beneficiary isn't the Beneficiary of Fee");

			creditAmount = creditAmount.add(fee.getAmount());
			fee.setCreditAddress(AddressUtils.generateAddress(credit));
			fee.setState(Fee.State.CREDITED);
			setStateEntity(fee);
		}
		logger.info("Credit-Amount: " + creditAmount);
		logger.info("Beneficiary: " + beneficiary);
		logger.info("Originator: " + originator);
		if(!isRoCredit){
			beneficiary.setBalance(beneficiary.getBalance().subtract(creditAmount));
			setStateEntity(beneficiary);

			originator.setBalance(originator.getBalance().add(creditAmount));
			setStateEntity(originator);
		}else{
			beneficiary.setBalance(beneficiary.getBalance().subtract(creditAmount));
			setStateEntity(beneficiary);
		}
		logger.info("Balances: ");
		logger.info("Beneficiary: " + beneficiary.getBalance());
		logger.info("Originator: " + originator.getBalance());
		credit.setAmount(creditAmount);
		setStateEntity(credit);
	}

	private void handleAddPayment(AddPaymentReq request, TpProcessRequest transactionRequest) throws InvalidTransactionException, InternalError {
		if(!request.getAction().equals(Actions.ADD_ACTION)) throw new InvalidTransactionException("False Action for Payment-Request");
		final User originator = UserUtils.getUserByPublicKey(getApi(), transactionRequest.getHeader().getSignerPublicKey());
		if(originator == null) throw new InvalidTransactionException("Originator can't be found");

		checkRoyaltyOrganisation(originator);
		final Invoice invoice = InvoiceUtils.getInvoiceByNoAndOriginator(getApi(), request.getInvoiceNo(), originator.getPublicKeyHex());
		if(invoice == null) throw new InvalidTransactionException("Invoice for can't be found");

		for(String feeAddress : invoice.getFeeAddresses()){
			final Fee fee = new Fee(StateUtils.getStateForAddress(getStateStore(), feeAddress));
			if(fee.getState()!= Fee.State.INVOICED) throw new InvalidTransactionException("Only invoiced Fees can be payed");
			fee.setState(Fee.State.PAYED);
			StateUtils.setState(getStateStore(), feeAddress, fee.serializePayload());
		}

		final User recipient = new User(StateUtils.getStateForAddress(getStateStore(), invoice.getRecipientAddress()));

		recipient.setBalance(recipient.getBalance().add(invoice.getAmount()));
		originator.setBalance(originator.getBalance().subtract(invoice.getAmount()));

		invoice.setState(Invoice.State.Payed);

		StateUtils.setState(getStateStore(), AddressUtils.generateAddress(invoice), invoice.serializePayload());
	}

	private void handleAddInvoice(AddInvoiceReq request, TpProcessRequest tpRequest) throws InvalidTransactionException, InternalError {
		if(!request.getAction().equals(Actions.ADD_ACTION)) throw new InvalidTransactionException("False Action for Invoice-Request");
		final User originator = UserUtils.getUserByPublicKey(getApi(), tpRequest.getHeader().getSignerPublicKey());
		final String originatorAddress = AddressUtils.generateAddress(originator);
		final String recipientAddress = request.getRecipientAddress();
		logger.info("OriginatorAddress: " + originatorAddress);
		logger.info("RecipientAddress: "  + recipientAddress);
		final User recipient = new User(getState(recipientAddress));

		if(originator == null) throw new InvalidTransactionException("Originator is no valid User");
		checkRoyaltyOrganisation(originator);
		checkDistributor(recipient);
		if(InvoiceUtils.getInvoiceByNoAndOriginator(getApi(), request.getInvoiceNo(), originator.getPublicKeyHex()) != null){
			throw new InvalidTransactionException("Invoice with invoiceNo for the originator already exists");
		}

		final List<String> feeAddresses = request.getFeeAddresses();
		final Invoice invoice = new Invoice(request.getInvoiceNo(), recipientAddress, originatorAddress, feeAddresses, Invoice.State.Open, null);
		final String invoiceAddress = AddressUtils.generateAddress(invoice);

		BigDecimal invoiceAmount = BigDecimal.ZERO;
		for(final String feeAddress : feeAddresses) {
			final Fee fee = checkFeeForInvoice(feeAddress, originatorAddress, recipientAddress);
			fee.setState(Fee.State.INVOICED);
			fee.setInvoiceAddress(invoiceAddress);
			StateUtils.setState(getStateStore(), AddressUtils.generateAddress(fee), fee.serializePayload());

			invoiceAmount = invoiceAmount.add(fee.getAmount());
		}
		invoice.setAmount(invoiceAmount);
		StateUtils.setState(getStateStore(), invoiceAddress, invoice.serializePayload());
		logger.info("Writing Invoice in State at address " + invoiceAddress);
	}

	private Fee checkFeeForInvoice(final String feeAddress, final String originatorAddress, final String recipientAddress) throws InvalidTransactionException, InternalError {
		final Fee fee = FeeUtils.getFeeFromState(getStateStore(), feeAddress);
		final Usage usage = new Usage(StateUtils.getStateForAddress(getStateStore(), fee.getUsageAddress()));
		final Tariff tariff = new Tariff(StateUtils.getStateForAddress(getStateStore(), usage.getTariffAddress()));
		final User benificiary = new User(StateUtils.getStateForAddress(getStateStore(), fee.getBeneficiaryAddress()));

		if(benificiary.getAddressRoyaltyOrganisation() != null && !benificiary.getAddressRoyaltyOrganisation().equals(originatorAddress)){
			throw new InvalidTransactionException("Originator isn't the accounting Royalty Organisation of the Fee: " + feeAddress);
		}

		if(!tariff.getDistributorAddress().equals(recipientAddress)){
			throw new InvalidTransactionException("Recipient isn't the valid DISTRIBUTOR of the Fee: " + feeAddress);
		}

		if(fee.getState()!=Fee.State.OPEN){
			throw new InvalidTransactionException("Fee isn't open");
		}
		return fee;
	}

	/**
	 * Method tries to identify Request for this TransactionFamily
	 * @return
	 * @throws InvalidTransactionException
	 */
	private AbstractRequest identifyRequest() throws InvalidTransactionException{
		final String payload = getRequestPayload();


		try{
			return getMapper().readValue(payload, AddPaymentReq.class);
		}catch (IOException e){
			logger.info("Request is no AddPaymentRequest");
		}

		try{
			return getMapper().readValue(payload, AddCreditReq.class);
		} catch (IOException e) {
			logger.info("Request is no AddCreditRequest");
		}

		try {
			return getMapper().readValue(payload, AddInvoiceReq.class);
		} catch (IOException e) {
			logger.info("Request is no AddInvoiceReq");
			throw new InvalidTransactionException("Request invalid!");
		}

	}
}
