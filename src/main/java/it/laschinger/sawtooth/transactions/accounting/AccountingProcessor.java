package it.laschinger.sawtooth.transactions.accounting;

import sawtooth.sdk.processor.TransactionProcessor;

public class AccountingProcessor {

	public static void main(String[] args) {
		TransactionProcessor transactionProcessor = new TransactionProcessor(args[0]);
		transactionProcessor.addHandler(new AccountingHandler());
		Thread thread = new Thread(transactionProcessor);
		thread.start();
	}
}
