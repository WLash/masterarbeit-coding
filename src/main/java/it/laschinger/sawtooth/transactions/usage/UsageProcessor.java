package it.laschinger.sawtooth.transactions.usage;

import it.laschinger.sawtooth.transactions.tariff.TariffHandler;
import sawtooth.sdk.processor.TransactionProcessor;

public class UsageProcessor {

	public static void main(String[] args) {
		TransactionProcessor transactionProcessor = new TransactionProcessor(args[0]);
		transactionProcessor.addHandler(new UsageHandler());
		Thread thread = new Thread(transactionProcessor);
		thread.start();
	}
}
