package it.laschinger.sawtooth.transactions.usage;

import it.laschinger.sawtooth.domain.creation.Creation;
import it.laschinger.sawtooth.domain.creation.CreationShare;
import it.laschinger.sawtooth.domain.entities.Fee;
import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.request.usage.AddUsageRequest;
import it.laschinger.sawtooth.transactions.AbstractHandler;
import it.laschinger.sawtooth.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UsageHandler extends AbstractHandler {

	public final static String FAMILY_NAME = "usage";
	public final static String VERSION = "1.0";

	public final static BigDecimal QUOTIENT_RO_SHARE = new BigDecimal(15);

	private final static Logger logger = LoggerFactory.getLogger(AbstractHandler.class);

	public UsageHandler() {
		super(FAMILY_NAME, VERSION);
	}


	@Override
	public void apply(TpProcessRequest transactionRequest, State state)
			throws InvalidTransactionException, InternalError {
		super.apply(transactionRequest, state);

		final AddUsageRequest request = parseRequest();

		logger.debug("TariffAddress: " + request.getTariffAddress());
		final Tariff tariff = new Tariff(getState(request.getTariffAddress()));
		if(tariff==null) {
			throw new InvalidTransactionException(
					"Referenced Tariff not found");
		}

		final String tariffAddress = AddressUtils.generateAddress(tariff);
		final Usage usage =
				new Usage(tariffAddress,
						request.getCreationId(),
						request.getTime(),
						request.getText());
		final String usageAddress = AddressUtils.generateAddress(usage);

		final User distributor = UserUtils
				.getUserByPublicKey(
						getApi(),
						transactionRequest.getHeader().getSignerPublicKey());
		if(distributor == null){
			throw new InvalidTransactionException(
					"Request Creator is not the valid Distributor of Tariff");
		}
		checkDistributor(distributor);

		final Creation creation = CreationUtils.getCreationById(request.getCreationId());
		if(creation == null) {
			throw new InvalidTransactionException("Creation not found");
		}

		final Set<CreationShare> shares = creation.getShareSet();

		final List<Fee> fees = new ArrayList<>();
		final BigDecimal amountForRo = BigDecimalUtils.
				calcQuotient(tariff.getAmount(), QUOTIENT_RO_SHARE);
		final Fee feeForRo =
				new Fee(
						usageAddress,
						tariff.getRoyaltyOrganisationAddress(),
						null,
						null,
						amountForRo,
						Fee.State.OPEN);

		fees.add(feeForRo);
		final BigDecimal amountForShareHolder =
				tariff.getAmount().subtract(amountForRo);

		logger.debug("Creation: " + creation);
		logger.debug("Shares-Size: " + shares.size());
		BigDecimal allShares = BigDecimal.ZERO;
		for(final CreationShare share : shares){
			final User user = UserUtils
					.getUserByCreationDbId(getApi(), share.getShareHolder().getId());
			if(user == null) {
				throw new InvalidTransactionException(
						"User of Shareholder can't be found");
			}
			final String userAddress = AddressUtils.generateAddress(user);
			logger.debug("AmountForShareholder: " + amountForShareHolder);
			final BigDecimal amountForFee = amountForShareHolder
					.divide(BigDecimal.valueOf(100))
					.multiply(share.getQuotient());
			logger.debug("AmountForFee: " + amountForFee);
			final Fee fee =
					new Fee(usageAddress, userAddress, null, null, amountForFee, Fee.State.OPEN);
			fees.add(fee);
			allShares = allShares.add(share.getQuotient());
		}
		logger.debug("AllShares: " + allShares);
		if(!(BigDecimalUtils.equalValue(allShares, new BigDecimal(100)))){
			throw new InvalidTransactionException("All Share-Quotiens added have to be 100");
		}


		BigDecimal amountDistributor = BigDecimal.ZERO;
		for(final Fee fee : fees){
			final String feeAddress = AddressUtils.generateAddress(fee);
			StateUtils.setState(state, feeAddress, fee.serializePayload());

			final User user = UserUtils
					.getUserByAddress(getApi(), fee.getBeneficiaryAddress());
			user.setBalance(user.getBalance().add(fee.getAmount()));
			StateUtils
					.setState(state, fee.getBeneficiaryAddress(), user.serializePayload());
			amountDistributor = amountDistributor.add(fee.getAmount());
		}
		logger.debug("AmountUser: " + amountDistributor);
		logger.debug("Fees-Size: " + fees.size());
		logger.debug("UsageAddress: " + usageAddress);

		distributor.setBalance(distributor.getBalance().subtract(amountDistributor));

		final String distributorAddress =
				AddressUtils.generateAddress(distributor);
		setState(distributorAddress, distributor.serializePayload());
		setState(usageAddress, usage.serializePayload());
	}

	private AddUsageRequest parseRequest() throws InvalidTransactionException {
		try {
			final AddUsageRequest req = getMapper().readValue(getRequestPayload(), AddUsageRequest.class);
			logger.debug("Request: " + req);
			if(!req.getAction().equals(Actions.ADD_ACTION)) throw new InvalidTransactionException("invalid Action: " + req.getAction());
			return req;
		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidTransactionException("Request is no AddUsageRequest");
		}
	}
}
