package it.laschinger.sawtooth.transactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.laschinger.sawtooth.domain.entities.Entity;
import it.laschinger.sawtooth.domain.entities.EntityI;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.TransactionHandler;
import sawtooth.sdk.processor.Utils;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;

public class AbstractHandler implements TransactionHandler {

	private final static Logger logger = LoggerFactory.getLogger(AbstractHandler.class);

	private final String familyName;
	private final String version;
	private String nameSpace;
	private String requestPayload;
	private ObjectMapper mapper;
	private String signerPublicKey;
	private State stateStore;
	private RestApi api;

	public AbstractHandler(final String familyName, final String version){
		this.familyName = familyName;
		this.version = version;
		byte[] familyNameBytes = null;
		try{
			familyNameBytes = this.transactionFamilyName().getBytes(GlobalUtils.UTF_8);
		}catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		this.nameSpace = Utils.hash512(familyNameBytes).substring(0,6);
		api = new RestApi("localhost", "8008", null);
	}

	@Override
	public String transactionFamilyName() {
		return familyName;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public Collection<String> getNameSpaces() {
		return Collections.singletonList(nameSpace);
	}

	@Override
	public void apply(TpProcessRequest transactionRequest, State state)
			throws InvalidTransactionException, InternalError {
		logger.debug("Transaction for " + familyName);
		logger.debug("Inputs: " + transactionRequest.getHeader().getInputsList());
		logger.debug("Outputs: " + transactionRequest.getHeader().getOutputsList());
		logger.debug("Signer-Pub: " + transactionRequest.getHeader().getSignerPublicKey());
		final String payload = GlobalUtils.decodePayload(transactionRequest);
		logger.debug("Payload Extracted: " + payload);
		logger.debug("CurrentTime in millis: " + System.currentTimeMillis());
		logger.debug("Dependencies: " + transactionRequest.getHeader().getDependenciesList());
		this.requestPayload = payload;
		this.mapper = new ObjectMapper();
		this.signerPublicKey = transactionRequest.getHeader().getSignerPublicKey();
		this.stateStore = state;

	}

	public String getRequestPayload(){ return requestPayload; }

	public String getNameSpace() {
		return nameSpace;
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public String getSignerPublicKey() {
		return signerPublicKey;
	}

	public State getStateStore() {
		return stateStore;
	}

	public void checkValueBiggerZero(final BigDecimal value)
			throws InvalidTransactionException {
		if(BigDecimalUtils.isValueLessZero(value)){
			throw new InvalidTransactionException("Value less than zero");
		}
	}

	public String getState(final String address)
			throws InvalidTransactionException, InternalError {
		return StateUtils.getStateForAddress(getStateStore(), address);
	}

	public <T extends Entity & EntityI> void setStateEntity(final T entity)
			throws InvalidTransactionException, InternalError {
		StateUtils.setState(getStateStore(),
				AddressUtils.generateAddress(entity),
				entity.serializePayload());
	}

	public void setState(final String address, final String payload)
			throws InternalError, InvalidTransactionException {
		StateUtils.setState(getStateStore(), address, payload);
	}

	public boolean isAddressExisting(final String address)
			throws InvalidTransactionException, InternalError {
		logger.debug("checking address: " + address);

		final String state = getState(address);
		logger.debug("state: " + state);
		return state != null && !state.isEmpty();
	}

	public void checkExistingAddress(final String address)
			throws InvalidTransactionException, InternalError {

		if(isAddressExisting(address)){
			throw new InvalidTransactionException(
				"Address already exists: " + address + ")");
		}
	}

	public void checkDistributor(final User distributor)
			throws InvalidTransactionException {
		if (!distributor.isDistributor()) {
			throw new InvalidTransactionException(
				"DISTRIBUTOR is no valid DISTRIBUTOR");
		}
		checkUserState(distributor);
	}

	public void checkRoyaltyOrganisation(final User royaltyOrganisation)
			throws InvalidTransactionException {
		if(!royaltyOrganisation.isRoyaltyOrganisation()){
			throw new InvalidTransactionException(
				"ROYALTY_ORGANISATION is no valid ROYALTY_ORGANISATION");
		}
		checkUserState(royaltyOrganisation);
	}

	public void checkPublisher(final User publisher) throws InvalidTransactionException {
		if (!publisher.isPublisher()) {
			throw new InvalidTransactionException(
				"PUBLISHER is no valid PUBLISHER");
		}
		checkUserState(publisher);
	}

	public void checkArtist(final User artist) throws InvalidTransactionException {
		if(artist.isArtist()){
			throw new InvalidTransactionException(
				"ARTIST is no valid ARTIST");
		}
		checkUserState(artist);
	}

	public void checkUserState(final User user) throws InvalidTransactionException {
		if(user.getState() != User.State.CONFIRMED){
			throw new InvalidTransactionException(
				"User " + user.getName() + " is not confirmed");
		}
	}

	public RestApi getApi() {
		return api;
	}
}
