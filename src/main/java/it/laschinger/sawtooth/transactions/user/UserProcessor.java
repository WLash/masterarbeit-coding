package it.laschinger.sawtooth.transactions.user;

import sawtooth.sdk.processor.TransactionProcessor;

public class UserProcessor {

	public static void main(String[] args) {

		TransactionProcessor transactionProcessor = new TransactionProcessor(args[0]);
		transactionProcessor.addHandler(new UserHandler());
		Thread thread = new Thread(transactionProcessor);
		thread.start();
	}
}
