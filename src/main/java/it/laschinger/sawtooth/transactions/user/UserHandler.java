package it.laschinger.sawtooth.transactions.user;

import it.laschinger.sawtooth.domain.creation.CreationUser;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.request.AbstractRequest;
import it.laschinger.sawtooth.request.user.AddUserReq;
import it.laschinger.sawtooth.request.user.ChangeUserStateReq;
import it.laschinger.sawtooth.transactions.AbstractHandler;
import it.laschinger.sawtooth.utils.AddressUtils;
import it.laschinger.sawtooth.utils.CreationUtils;
import it.laschinger.sawtooth.utils.StateUtils;
import it.laschinger.sawtooth.utils.UserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class UserHandler extends AbstractHandler {

	private final Logger logger = LoggerFactory.getLogger(UserHandler.class);
	public final static String FAMILY_NAME = "user";
	public final static String VERSION = "1.0";


	public UserHandler(){
		super(FAMILY_NAME, VERSION);
	}

	@Override
	public void apply(TpProcessRequest transactionRequest, State stateStore) throws InvalidTransactionException, InternalError {
		super.apply(transactionRequest, stateStore);
		logger.debug("Request-Hash: " + transactionRequest.hashCode());
		final AbstractRequest abstractRequest = identifyRequest();

		if(abstractRequest instanceof AddUserReq){
			handleAddUserRequest((AddUserReq) abstractRequest, stateStore);
		} else if(abstractRequest instanceof ChangeUserStateReq){
			handleChangeUserStateRequest((ChangeUserStateReq) abstractRequest);
		}
		logger.debug("___________________________________________________ END ________________________________________");
	}

	/**
	 * Method tries to identify Request for this TransactionFamily
	 * @return
	 * @throws InvalidTransactionException
	 */
	private AbstractRequest identifyRequest() throws InvalidTransactionException{
		final String payload = getRequestPayload();
		AbstractRequest request;
		try {
			request = getMapper().readValue(payload, AddUserReq.class);
			logger.debug("AddUserRequest");
			return request;
		} catch (IOException e) {
		}

		try{
			request = getMapper().readValue(payload, ChangeUserStateReq.class);
			logger.debug("ChangeUserStateRequest");
			return request;
		}catch (IOException e){
			throw new InvalidTransactionException("Request invalid!");
		}
	}

	/**
	 * Processes a AddUserReq
	 * @param addReq
	 * @param stateStore
	 * @throws InvalidTransactionException
	 * @throws InternalError
	 */
	private void handleAddUserRequest(AddUserReq addReq, State stateStore) throws InvalidTransactionException, InternalError{
		try {
			User.State userState;
			if (addReq.getAction().equals("add")) {
				userState = User.State.INQUIRED;
			} else if (addReq.getAction().equals("init") && addReq.getRole().equals(User.Role.ROYALTY_ORGANISATION) /*&& UserUtils.isFirstUser(getApi() )*/) {
				userState = User.State.CONFIRMED;
			} else {
				throw new InvalidTransactionException("Wrong Action in Request!");
			}

			final User user = new User(addReq.getPublicKey(), addReq.getUserName(), BigDecimal.ZERO, userState, addReq.getRole(), addReq.getRoyaltyOrganisationAddress(), addReq.getCreationDbId());

			final boolean isPublisherOrArtist = user.isArtist() || user.isPublisher();

			if (isPublisherOrArtist) {
				if (user.getAddressRoyaltyOrganisation() == null)
					throw new InvalidTransactionException("ARTIST and PUBLISHER need ROYALTY_ORGANISATION!");
				if (user.getAddressRoyaltyOrganisation() != null) {

					final String entry = getState(user.getAddressRoyaltyOrganisation());
					try {
						final User royaltyOrganisation = new User(entry);
						if (royaltyOrganisation.getRole() != User.Role.ROYALTY_ORGANISATION) {
							throw new InvalidTransactionException("Referenced Royalty Organisation is not a Royalty Organisation!");
						}
					}catch (NullPointerException e){
						throw new InvalidTransactionException("RoyaltyOrganisation can't be found");
					}
				}
				if (user.getCreationDbId() != null && Integer.valueOf(user.getCreationDbId()).compareTo(0) > 0) {
					//CreationDbId vorhanden
					final CreationUser creationUser = CreationUtils.getCreationUserById(user.getCreationDbId());
					if (creationUser == null)
						throw new InvalidTransactionException("CreationDbUser of user " + user.getName() + " doesn't exist. CreationDbUserId: " + user.getCreationDbId());

					//CreationDbId hat noch kein anderer Benutzer
					final List<User> artistsAndPublisher = new ArrayList<>(UserUtils.getAllArtists(getApi()));
					artistsAndPublisher.addAll(UserUtils.getAllPublisher(getApi()));
					final boolean creationDbIdExists = UserUtils.hasUserCreationDbId(user.getPublicKeyHex(), artistsAndPublisher, user.getCreationDbId());

					if (creationDbIdExists)
						throw new InvalidTransactionException("CreationDbId is already mapped to another User");

				} else {
					throw new InvalidTransactionException("CreationDb is invalid for ARTIST/PUBLISHER");
				}
			} else {
				if (user.getAddressRoyaltyOrganisation() != null)
					throw new InvalidTransactionException("Royalty Organisations and DISTRIBUTOR must not have a Royalty Organisation!");
				if (user.getCreationDbId() != null)
					throw new InvalidTransactionException("Royalty Organisation and DISTRIBUTOR must not have a CreationDbId");
			}


			final String address = AddressUtils.generateAddress(user);

			logger.debug("Address: " + address);

			user.setBalance(BigDecimal.ZERO);
			StateUtils.setState(stateStore, address, user.serializePayload());
			logger.debug("Data has been written to " + address + ". ");
		}catch (Exception e){
			e.printStackTrace();
			throw new InvalidTransactionException(e.toString());
		}
	}


	private void handleChangeUserStateRequest(ChangeUserStateReq request) throws InvalidTransactionException, InternalError {
		final String action = request.getAction();
		final User.State newState;
		if(action.equals("confirm")){
			newState = User.State.CONFIRMED;
		}else if (action.equals("decline")){
			newState = User.State.DECLINED;
		}else{
			throw new InvalidTransactionException("Invalid Request Action");
		}

		logger.debug("Validate User");
		final User user;
		try {
			user = new User(getState(request.getUserAddress()));
		}catch (NullPointerException e){
			throw new InvalidTransactionException("User not found!");
		}

		logger.debug("Validatre RO");
		final User ro;
		try{
			ro = new User(getState(request.getRoAddress()));
		}catch (NullPointerException e){
			throw new InvalidTransactionException("Royalty Organisation not found!");
		}

		if(ro.getRole()!= User.Role.ROYALTY_ORGANISATION) throw new InvalidTransactionException("Royalty Organisation is no RO!");

		if(!ro.getPublicKeyHex().equals(getSignerPublicKey())){
			throw new InvalidTransactionException("Wrong Signature!");
		}
		user.setState(newState);
		setState(request.getUserAddress(), user.serializePayload());
	}
}
