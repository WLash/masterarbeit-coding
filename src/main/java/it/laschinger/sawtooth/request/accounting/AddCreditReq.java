package it.laschinger.sawtooth.request.accounting;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.request.AbstractRequest;

import java.util.List;

public class AddCreditReq extends AbstractRequest {


	@JsonProperty
	private String creditNo;

	@JsonProperty
	private String originatorAddress;

	@JsonProperty
	private String beneficiaryAddress;

	@JsonProperty
	private List<String> feeAddresses;

	public AddCreditReq(){}

	public AddCreditReq(String action, String creditNo, String originatorAddress, String beneficiaryAddress, List<String> feeAddresses) {
		super(action);
		this.creditNo = creditNo;
		this.originatorAddress = originatorAddress;
		this.beneficiaryAddress = beneficiaryAddress;
		this.feeAddresses = feeAddresses;
	}

	public String getCreditNo() {
		return creditNo;
	}

	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}

	public String getOriginatorAddress() {
		return originatorAddress;
	}

	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	public String getBeneficiaryAddress() {
		return beneficiaryAddress;
	}

	public void setBeneficiaryAddress(String beneficiaryAddress) {
		this.beneficiaryAddress = beneficiaryAddress;
	}

	public List<String> getFeeAddresses() {
		return feeAddresses;
	}

	public void setFeeAddresses(List<String> feeAddresses) {
		this.feeAddresses = feeAddresses;
	}
}
