package it.laschinger.sawtooth.request.accounting;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.request.AbstractRequest;

import java.util.List;

public class AddInvoiceReq extends AbstractRequest {

	@JsonProperty
	private String invoiceNo;

	@JsonProperty
	private String recipientAddress;

	@JsonProperty
	private List<String> feeAddresses;

	public AddInvoiceReq(){}

	public AddInvoiceReq(final String action, final String invoiceNo, final String recipientAddress, final List<String> feeAddresses){
		super(action);
		this.invoiceNo = invoiceNo;
		this.recipientAddress = recipientAddress;
		this.feeAddresses = feeAddresses;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getRecipientAddress() {
		return recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public List<String> getFeeAddresses() {
		return feeAddresses;
	}

	public void setFeeAddresses(List<String> feeAddresses) {
		this.feeAddresses = feeAddresses;
	}

}
