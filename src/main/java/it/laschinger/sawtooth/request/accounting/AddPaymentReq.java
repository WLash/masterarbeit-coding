package it.laschinger.sawtooth.request.accounting;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.request.AbstractRequest;

public class AddPaymentReq extends AbstractRequest {

	@JsonProperty
	private String invoiceNo;

	public AddPaymentReq(){}

	public AddPaymentReq(String action, String invoiceNo) {
		super(action);
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
}
