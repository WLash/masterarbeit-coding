package it.laschinger.sawtooth.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AbstractRequest {

	@JsonProperty
	private String action;

	public AbstractRequest(){}

	public AbstractRequest(final String action){
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
