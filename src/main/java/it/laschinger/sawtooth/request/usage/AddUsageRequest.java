package it.laschinger.sawtooth.request.usage;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.request.AbstractRequest;


public class AddUsageRequest extends AbstractRequest {

	@JsonProperty
	private String tariffAddress;

	@JsonProperty
	private String creationId;

	@JsonProperty
	private Long time;

	@JsonProperty
	private String text;

	public AddUsageRequest(){}

	public AddUsageRequest(final String action, final String tariffAddress, final String creationId, final Long time, final String text) {
		super(action);
		this.tariffAddress = tariffAddress;
		this.creationId = creationId;
		this.time = time;
		this.text = text;
	}

	public String getTariffAddress() {
		return tariffAddress;
	}

	public String getCreationId() {
		return creationId;
	}

	public Long getTime() {
		return time;
	}

	public String getText() {
		return text;
	}

	public void setTariffAddress(String tariffAddress) {
		this.tariffAddress = tariffAddress;
	}

	public void setCreationId(String creationId) {
		this.creationId = creationId;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public void setText(String text) {
		this.text = text;
	}
}
