package it.laschinger.sawtooth.request.tariff;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.request.AbstractRequest;

import java.math.BigDecimal;

public class AddTariffReq extends AbstractRequest {

	@JsonProperty
	private String distributorAddress;

	@JsonProperty
	private String royaltyOrganisationAddress;

	@JsonProperty
	private Usage.Type usageType;

	@JsonProperty
	private BigDecimal amount;

	public AddTariffReq(){}

	public AddTariffReq(final String action, final String distributorAddress, final String royaltyOrganisationAddress, final Usage.Type usageType, final BigDecimal amount){
		super(action);
		this.distributorAddress = distributorAddress;
		this.royaltyOrganisationAddress = royaltyOrganisationAddress;
		this.usageType = usageType;
		this.amount = amount;
	}

	public String getDistributorAddress() {
		return distributorAddress;
	}

	public void setDistributorAddress(String distributorAddress) {
		this.distributorAddress = distributorAddress;
	}

	public String getRoyaltyOrganisationAddress() {
		return royaltyOrganisationAddress;
	}

	public void setRoyaltyOrganisationAddress(String royaltyOrganisationAddress) {
		this.royaltyOrganisationAddress = royaltyOrganisationAddress;
	}

	public Usage.Type getUsageType() {
		return usageType;
	}

	public void setUsageType(Usage.Type usageType) {
		this.usageType = usageType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


}
