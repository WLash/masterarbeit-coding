package it.laschinger.sawtooth.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.request.AbstractRequest;

public class AddUserReq extends AbstractRequest {

	@JsonProperty
	private String userName;

	@JsonProperty
	private  String publicKey;

	@JsonProperty
	private User.Role role;

	@JsonProperty
	private String royaltyOrganisationAddress;

	@JsonProperty
	private String creationDbId;

	public AddUserReq(){}

	public AddUserReq(String action, String userName,
					  String publicKey, User.Role role,
					  String royaltyOrganisationAddress, String creationDbId) {
		super(action);
		this.userName = userName;
		this.publicKey = publicKey;
		this.role = role;
		this.royaltyOrganisationAddress = royaltyOrganisationAddress;
		this.creationDbId = creationDbId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public User.Role getRole() {
		return role;
	}

	public void setRole(User.Role role) {
		this.role = role;
	}

	public String getRoyaltyOrganisationAddress() {
		return royaltyOrganisationAddress;
	}

	public void setRoyaltyOrganisationAddress(String royaltyOrganisationAddress) {
		this.royaltyOrganisationAddress = royaltyOrganisationAddress;
	}

	public String getCreationDbId() {
		return creationDbId;
	}

	public void setCreationDbId(String creationDbId) {
		this.creationDbId = creationDbId;
	}
}
