package it.laschinger.sawtooth.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.laschinger.sawtooth.request.AbstractRequest;

public class ChangeUserStateReq extends AbstractRequest {

	@JsonProperty
	private String userAddress;

	@JsonProperty
	private String roAddress;


	public ChangeUserStateReq(){}

	public ChangeUserStateReq(final String action, final String userAddress, final String roAddress){
		super(action);
		this.userAddress = userAddress;
		this.roAddress = roAddress;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getRoAddress() {
		return roAddress;
	}

	public void setRoAddress(String roAddress) {
		this.roAddress = roAddress;
	}
}
