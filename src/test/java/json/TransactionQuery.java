package json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(value = { "paging", "link" })
public class TransactionQuery {

	@JsonProperty
	String head;

	@JsonProperty("data")
	private List<TransactionEntry> entries = new ArrayList<>();


	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public List<TransactionEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<TransactionEntry> entries) {
		this.entries = entries;
	}
}
