package json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(value = { "paging", "link" })
public class BlocksQuery {
	@JsonProperty
	private String head;

	@JsonProperty("data")
	private List<BlockEntry> entries = new ArrayList<>();


	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public List<BlockEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<BlockEntry> entries) {
		this.entries = entries;
	}
}
