package json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties({"header", "trace"})
public class BatchEntry {

	@JsonProperty("header_signature")
	private String headerSignature;

	@JsonProperty
	private List<TransactionEntry> transactions;

	public String getHeaderSignature() {
		return headerSignature;
	}

	public void setHeaderSignature(String headerSignature) {
		this.headerSignature = headerSignature;
	}

	public List<TransactionEntry> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionEntry> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "BatchEntry{" +
				"headerSignature='" + headerSignature + '\'' +
				", transactions=" + transactions +
				'}';
	}
}
