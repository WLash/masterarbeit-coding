package json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(value = { "header"})
public class BlockEntry {
	@JsonProperty("header_signature")
	private	String headerSignature;

	@JsonProperty
	private List<BatchEntry> batches;


	public String getHeaderSignature() {
		return headerSignature;
	}

	public void setHeaderSignature(String headerSignature) {
		this.headerSignature = headerSignature;
	}

	public List<BatchEntry> getBatches() {
		return batches;
	}

	public void setBatches(List<BatchEntry> batches) {
		this.batches = batches;
	}

	@Override
	public String toString() {
		return "TransactionEntry{" +
				"headerSignature='" + headerSignature + '\'' +
				'}';
	}
}
