package json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = { "header"})
public class TransactionEntry {

	@JsonProperty("header_signature")
	String headerSignature;

	@JsonProperty
	String payload;


	public String getHeaderSignature() {
		return headerSignature;
	}

	public void setHeaderSignature(String headerSignature) {
		this.headerSignature = headerSignature;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "TransactionEntry{" +
				"headerSignature='" + headerSignature + '\'' +
				", payload='" + payload + '\'' +
				'}';
	}
}
