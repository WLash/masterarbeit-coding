package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.laschinger.sawtooth.domain.creation.CreationUser;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.request.user.AddUserReq;
import it.laschinger.sawtooth.request.user.ChangeUserStateReq;
import it.laschinger.sawtooth.utils.Actions;
import it.laschinger.sawtooth.utils.AddressUtils;
import it.laschinger.sawtooth.utils.TransactionUtils;
import org.bitcoinj.core.ECKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.protobuf.Transaction;
import testData.KeyUserTuple;
import testData.UserTestData;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserTestUtils {
	private final static Logger logger = LoggerFactory.getLogger(UserTestUtils.class);

	public final static Map<String, UserTestData> roTestDataMap = createRoMap();
	public final static Map<String, UserTestData> distributorTestDataMap = createDistributorMap();
	public final static Map<String, UserTestData> publisherTestDataMap = createPublisherMap();
	public final static Map<String, UserTestData> artistTestDataMap = createArtistMap();

	public final static String ROYALTY_ORGANISATION_1 = "RoyaltyOrganisation_1";
	public final static String ROYALTY_ORGANISATION_2 = "RoyaltyOrganisation_2";
	public final static String ROYALTY_ORGANISATION_3 = "RoyaltyOrganisation_3";
	public final static String DISTRIBUTOR_1 = "Distributor_1";
	public final static String DISTRIBUTOR_2 = "Distributor_2";
	public final static String DISTRIBUTOR_3 = "Distributor_3";
	public final static String PUBLISHER_1 = "Publisher_1";
	public final static String PUBLISHER_2 = "Publisher_2";
	public final static String PUBLISHER_3 = "Publisher_3";
	public final static String PUBLISHER_4 = "Publisher_4";
	public final static String PUBLISHER_5 = "Publisher_5";
	public final static String ARTIST_1 = "Artist_1";
	public final static String ARTIST_2 = "Artist_2";
	public final static String ARTIST_3 = "Artist_3";
	public final static String ARTIST_4 = "Artist_4";
	public final static String ARTIST_5 = "Artist_5";

	private static Map<String, UserTestData> createRoMap() {
		final Map<String, UserTestData> result = new HashMap<>();
		result.put(ROYALTY_ORGANISATION_1, new UserTestData(ECKey.fromPrivate(new BigInteger("105381931323033539242570969186215999685244199760638517853302677973845499619317")), User.Role.ROYALTY_ORGANISATION));
		result.put(ROYALTY_ORGANISATION_2, new UserTestData(ECKey.fromPrivate(new BigInteger("7178780912752062543319711271388682181843985748165836418471374530323791862852")), User.Role.ROYALTY_ORGANISATION));
		result.put(ROYALTY_ORGANISATION_3, new UserTestData(ECKey.fromPrivate(new BigInteger("24101392938157655552998147072083432093750968686005821328033378891786755416081")), User.Role.ROYALTY_ORGANISATION));
		return result;
	}

	public static Map<String, UserTestData> createDistributorMap(){
		final Map<String, UserTestData> result = new HashMap<>();
		result.put(DISTRIBUTOR_1, new UserTestData(ECKey.fromPrivate(new BigInteger("34590008870940898987543259513455671490442704158648287328378911696132425419303")), User.Role.DISTRIBUTOR));
		result.put(DISTRIBUTOR_2, new UserTestData(ECKey.fromPrivate(new BigInteger("19955451880118590022816692690641185051876829953242887039534100314468202962615")), User.Role.DISTRIBUTOR));
		result.put(DISTRIBUTOR_3, new UserTestData(ECKey.fromPrivate(new BigInteger("29438743279564949114800164141102136608046863869498899591878927368923936562886")), User.Role.DISTRIBUTOR));
		return result;
	}

	private static Map<String, UserTestData> createPublisherMap(){
		final Map<String, UserTestData> result = new HashMap<>();
		result.put(PUBLISHER_1, new UserTestData(ECKey.fromPrivate(new BigInteger("83148033605496803385517944966124874766236144488971581481906999927016814903160")), User.Role.PUBLISHER));
		result.put(PUBLISHER_2, new UserTestData(ECKey.fromPrivate(new BigInteger("57839818467642262017656112560194891077716840544969132399658172988994820520286")), User.Role.PUBLISHER));
		result.put(PUBLISHER_3, new UserTestData(ECKey.fromPrivate(new BigInteger("22686709922129826135781136919059973288177002688921443129624099641933960915866")), User.Role.PUBLISHER));
		result.put(PUBLISHER_4, new UserTestData(ECKey.fromPrivate(new BigInteger("50100296034509807679435580971795603430021241768508805001679728561324189051560")), User.Role.PUBLISHER));
		result.put(PUBLISHER_5, new UserTestData(ECKey.fromPrivate(new BigInteger("92545429388914681433143264667613091175761528222594392500800756062607273961740")), User.Role.PUBLISHER));
		return result;
	}

	private static Map<String, UserTestData> createArtistMap(){
		final Map<String, UserTestData> result = new HashMap<>();
		result.put(ARTIST_1, new UserTestData(ECKey.fromPrivate(new BigInteger("107254226847255171300791789208219918225348038077518204622596655069697181017874")), User.Role.ARTIST));
		result.put(ARTIST_2, new UserTestData(ECKey.fromPrivate(new BigInteger("36231850331442821135270591582389074190074961338180936577724851083989515025217")), User.Role.ARTIST));
		result.put(ARTIST_3, new UserTestData(ECKey.fromPrivate(new BigInteger("45220012145242993322760206606686932313885786485845690063684329286152965026606")), User.Role.ARTIST));
		result.put(ARTIST_4, new UserTestData(ECKey.fromPrivate(new BigInteger("17613283472760796511446757753088680689158414176400424623845047048053169556013")), User.Role.ARTIST));
		result.put(ARTIST_5, new UserTestData(ECKey.fromPrivate(new BigInteger("50472796312721926674870568308397400227258425552711306538729059658165092805635")), User.Role.ARTIST));
		return result;
	}



	public static KeyUserTuple initUserByUserAndRoName(final String userName, final User ro, final boolean isInit, final List<Transaction> transactions, final String... dependencies) throws Exception {

		final UserTestData userTestData = getUserTestDataByName(userName);
		final ECKey keyUser = userTestData.getKey();
		final User.Role userRole = userTestData.getRole();

		final boolean isArtistOrPublisher = userRole.equals(User.Role.ARTIST) || userRole.equals(User.Role.PUBLISHER);
		final String creationUserId = isArtistOrPublisher ?
			CreationDbTestUtils.testCreateUser(new CreationUser(userName + System.currentTimeMillis())).getId() : null;
		final String roAddress = !isArtistOrPublisher ? null : AddressUtils.generateAddress(ro);

		final User user = new User(keyUser.getPublicKeyAsHex(), userName, BigDecimal.ZERO, isInit ? User.State.INQUIRED : User.State.CONFIRMED, userRole, roAddress, creationUserId);
		user.addDependencies(dependencies);


		final User requestedUser = UserTestUtils.createUser(user, keyUser, isInit, transactions);
		final KeyUserTuple userTuple = new KeyUserTuple(userTestData.getKey(), requestedUser);

		return userTuple;
	}

	public static UserTestData getUserTestDataByName(final String userName){
		UserTestData userTestData = roTestDataMap.get(userName);
		if(userTestData == null){
			userTestData = distributorTestDataMap.get(userName);
		}
		if(userTestData == null){
			userTestData = publisherTestDataMap.get(userName);
		}
		if(userTestData == null){
			userTestData = artistTestDataMap.get(userName);
		}
		return userTestData;
	}



	/*____________________________________________________________________________________________________________________________*/

	public static KeyUserTuple getDefaultRo() throws IOException {
		return getUserByNameInMap(ROYALTY_ORGANISATION_1, roTestDataMap);
	}


	public static KeyUserTuple getDefaultDistributor() throws IOException {
		return getUserByNameInMap(DISTRIBUTOR_1, distributorTestDataMap);
	}


	public static KeyUserTuple getUserByNameInMap(final String userName, final Map<String, UserTestData> tuple) throws IOException {
		final UserTestData userData = tuple.get(userName);
		if(userData == null) return null;
		final ECKey key = userData.getKey();
		final User.Role role = userData.getRole();
		String creationDbId = null;
		if(role.equals(User.Role.ARTIST) || role.equals(User.Role.PUBLISHER)){
			creationDbId = CreationDbTestUtils.createRandomUser().getId();
		}
		final User user = new User(key.getPublicKeyAsHex(), userName, BigDecimal.ZERO, User.State.CONFIRMED, role, null, creationDbId);
		return new KeyUserTuple(userData.getKey(), user);	}


	public static User createUser(final User user, ECKey key, final boolean isInit, final List<Transaction> transactions){
		try {
			final String publicKeyHex = key.getPublicKeyAsHex();

			final AddUserReq addUserReq = new AddUserReq(isInit ? "init" : "add", user.getName(),
					publicKeyHex, user.getRole(), user.getAddressRoyaltyOrganisation(),
					user.getCreationDbId());
			final ObjectMapper mapper = new ObjectMapper();
			final String payload = mapper.writeValueAsString(addUserReq);

			logger.info("createUser: " + user.getDependencies());
			final Transaction transaction = createUserTransaction(key, payload, user.getDependencies());
			user.setTransactionHeader(transaction.getHeaderSignature());
			transactions.add(transaction);
			TestUtils.testRequestInSingleBatch(transaction, key);
			return user;
		}catch (JsonProcessingException e){
			e.printStackTrace();
			return null;
		}
	}



	public static Transaction createUserConfirmTransaction(final User user, final User ro, final ECKey privateKeyRo, final String... dependencies) throws JsonProcessingException {
		final ChangeUserStateReq req = new ChangeUserStateReq(Actions.CONFIRM_ACTION, AddressUtils.generateAddress(user), AddressUtils.generateAddress(ro));
		user.setState(User.State.CONFIRMED);
		return createUserTransaction(privateKeyRo, new ObjectMapper().writeValueAsString(req), Arrays.stream(dependencies).collect(Collectors.toList()));
	}

	private static Transaction createUserTransaction(ECKey privateKey, final String payload, final List<String> dependencies){
		final Transaction txn = TransactionUtils.getTxnUserWithDependencies(privateKey, payload, dependencies);
		return txn;
	}
}
