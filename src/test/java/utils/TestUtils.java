package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import it.laschinger.sawtooth.domain.entities.Entity;
import it.laschinger.sawtooth.domain.entities.EntityI;
import it.laschinger.sawtooth.domain.state.AddressState;
import it.laschinger.sawtooth.utils.*;
import json.BlockEntry;
import json.BlocksQuery;
import json.TransactionEntry;
import json.TransactionQuery;
import org.awaitility.Duration;
import org.bitcoinj.core.ECKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.protobuf.Batch;
import sawtooth.sdk.protobuf.BatchList;
import sawtooth.sdk.protobuf.Transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class TestUtils {

	private final static Logger logger = LoggerFactory.getLogger(TestUtils.class);

	public static List<RestApi> getAllApis(){
		final List<RestApi> restApis = new ArrayList<>();
		restApis.add(new RestApi("192.168.8.110", null, "webapps"));
		restApis.add(new RestApi("192.168.8.109", null, "webapps"));
		restApis.add(new RestApi("192.168.8.108", null, "webapps"));
		restApis.add(new RestApi("192.168.8.107", null, "webapps"));
		return restApis;
	}

	public static RestApi getApi(){
		return TestUtils.getRandomElementOfList(getAllApis());
	}

	public static <T extends Entity & EntityI> boolean testEntityCreated(final T entity) throws Exception {
		final String payload = entity.serializePayload();
		final String entityAddress = AddressUtils.generateAddress(entity);
		final AtomicBoolean bool = new AtomicBoolean(false);
		AwaitilityUtils.testAwaitilityUntilTrue(() ->{
			logger.info("startEntityCreatedTest");
			final AddressState state = StateUtils.getStateForAddressRest(getApi(), entityAddress);
			if(state == null) return false;
			final String statePayload = state.getData();
			logger.info("Entity-Test: " + payload);
			logger.info("Entity-BC: " + statePayload);
			logger.info("2: " + statePayload.equals(payload));
			bool.set(statePayload.equals(payload));
			return bool.get();
		}, new Duration(10L, TimeUnit.SECONDS));

		return bool.get();
	}

	public static <T extends Entity & EntityI> boolean testEntityCreated(final T entity, final RestApi api) throws Exception {
		final String payload = entity.serializePayload();
		final String entityAddress = AddressUtils.generateAddress(entity);
		final AtomicBoolean bool = new AtomicBoolean(false);
		AwaitilityUtils.testAwaitilityUntilTrue(() ->{
			logger.info("startEntityCreatedTest");
			final AddressState state = StateUtils.getStateForAddressRest(api, entityAddress);
			if(state == null) return false;
			final String statePayload = state.getData();
			logger.info("Entity-Test: " + payload);
			logger.info("Entity-BC: " + statePayload);
			logger.info("2: " + statePayload.equals(payload));
			bool.set(statePayload.equals(payload));
			return bool.get();
		}, new Duration(10L, TimeUnit.SECONDS));

		return bool.get();
	}

	public static void testRequestInSingleBatch(Transaction txn, ECKey key){
		final Batch batch = TransactionUtils.createBatch(key, Collections.singletonList(txn));
		final BatchList batchList = BatchList.newBuilder().addBatches(batch).build();
		AwaitilityUtils.testAwaitilityUntilTrue(() ->
			!sendBatches(batchList).contains("error"),
			Duration.TWO_MINUTES);

	}

	public static void testMultipleRequestsInSingleBatch(final List<Transaction> txns, ECKey key){
		final Batch batch = TransactionUtils.createBatch(key, txns);
		final BatchList batchList = BatchList.newBuilder().addBatches(batch).build();
		AwaitilityUtils.testAwaitilityUntilTrue(() ->
				!sendBatches(batchList).contains("error"),
				Duration.TEN_MINUTES);
	}

	public static String sendBatches(final BatchList batchList){
		logger.info("sendBatches");
		final ByteString batchBytes = batchList.toByteString();
		final String response = getApi().sendPostRequest(batchBytes.toByteArray(), "batches");
		return response;
	}

	public static String shuffleString(final String s){
		List<Character> characters = new ArrayList<>();
		for(char c:s.toCharArray()){
			characters.add(c);
		}
		final StringBuilder output = new StringBuilder(s.length());
		while(characters.size()!=0){
			int randPicker = (int)(Math.random()*characters.size());
			output.append(characters.remove(randPicker));
		}
		return output.toString();
	}

	public static boolean getRandomBoolean() {
		Random random = new Random();
		return random.nextBoolean();
	}

	public static <T> T getRandomElementOfList(final List<T> list) {
		Random rand = new Random();
		return list.get(rand.nextInt(list.size()));
	}

	public static List<TransactionEntry> getAllTransactions(final RestApi api) {
		final String json = api.sendGetRequest("transactions?limit=10000");
		final ObjectMapper mapper = new ObjectMapper();
		final AtomicReference<List<TransactionEntry>> entries = new AtomicReference<>();
		AwaitilityUtils.testAwaitilityUntilTrue(() -> {
			try {
				final TransactionQuery query = mapper.readValue(json, TransactionQuery.class);
				final List<TransactionEntry> list =query.getEntries();
				entries.set(list);

				return list != null && !list.isEmpty();

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}, Duration.TEN_SECONDS);
		return entries.get();
	}

	public static List<BlockEntry> getAllBlocks(final RestApi api) {
		final ObjectMapper mapper = new ObjectMapper();
		final AtomicReference<List<BlockEntry>> entries = new AtomicReference<>();
		AwaitilityUtils.testAwaitilityUntilTrue(() -> {
			try {
				final String json = api.sendGetRequest("blocks?limit=10000");
				final BlocksQuery query = mapper.readValue(json, BlocksQuery.class);
				final List<BlockEntry> list =query.getEntries();
				entries.set(list);

				return list != null && !list.isEmpty();

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}, Duration.TEN_SECONDS);
		return entries.get();
	}

}
