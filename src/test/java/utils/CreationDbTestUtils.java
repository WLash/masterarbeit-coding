package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import it.laschinger.sawtooth.domain.creation.Creation;
import it.laschinger.sawtooth.domain.creation.CreationShare;
import it.laschinger.sawtooth.domain.creation.CreationUser;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.utils.BigDecimalUtils;
import it.laschinger.sawtooth.utils.CreationUtils;
import it.laschinger.sawtooth.utils.RestApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;

public class CreationDbTestUtils {

	private final static Logger logger = LoggerFactory.getLogger(CreationDbTestUtils.class);
	private final static RestApi api = new RestApi("localhost", "8080", null);


	public static CreationUser testCreateUser(final CreationUser user) throws IOException {
		logger.info("New User: " + user);

		final ObjectMapper mapper = new ObjectMapper();
		final String json = mapper.writeValueAsString(user);
		logger.info("UserJson: " + json);
		final HttpResponse<JsonNode> response = api.sendJsonPostRequest(json, "user", "add");

		logger.info("Response: " + response.getBody());
		assert response.getStatus() == 200;
		final CreationUser savedCreationUser = mapper.readValue(response.getBody().toString(), CreationUser.class);
		assert savedCreationUser != null;
		assert user.getName().equals(savedCreationUser.getName());
		return savedCreationUser;
	}

	public static Creation testCreateCreation(final Creation creation) throws IOException {
		logger.info("New Creation: " + creation);

		final ObjectMapper mapper = new ObjectMapper();
		final String json = mapper.writeValueAsString(creation);
		logger.info("CreationJson: " + json);
		final HttpResponse<JsonNode> response = api.sendJsonPostRequest(json, "creation", "add");

		logger.info("Response: " + response.getBody());
		assert response.getStatus() == 200;
		final Creation savedCreation = mapper.readValue(response.getBody().toString(), Creation.class);
		assert savedCreation != null;
		assert creation.getTitle().equals(savedCreation.getTitle());
		return savedCreation;
	}

	public static CreationShare testCreateShare(final CreationShare share) throws IOException {
		logger.info("New Share: " + share);

		final ObjectMapper mapper = new ObjectMapper();
		final String json = mapper.writeValueAsString(share);
		logger.info("ShareJson: " + json);
		final HttpResponse<JsonNode> response = api.sendJsonPostRequest(json, "share", "add");

		logger.info("Response: " + response.getBody());
		assert response.getStatus() == 200;
		final CreationShare savedShare = mapper.readValue(response.getBody().toString(), CreationShare.class);
		assert savedShare != null;
		assert savedShare.getQuotient().equals(share.getQuotient());
		assert savedShare.getShareHolder().equals(share.getShareHolder());
		return savedShare;
	}

	public static CreationUser createRandomUser() throws IOException {
		final CreationUser creationUser = new CreationUser("test" + System.currentTimeMillis());
		return testCreateUser(creationUser);
	}

	public static Creation creatFullDataset() throws IOException {
		CreationUser creationUser = new CreationUser("test" + System.currentTimeMillis());
		creationUser = testCreateUser(creationUser);

		CreationUser creationUser2 = new CreationUser("test" + System.currentTimeMillis());
		creationUser2 = testCreateUser(creationUser2);

		CreationUser creationUser3 = new CreationUser("test" + System.currentTimeMillis());
		creationUser3 = testCreateUser(creationUser3);

		Creation creation = new Creation("title" + System.currentTimeMillis(), creationUser);
		creation = testCreateCreation(creation);

		CreationShare share = new CreationShare(creation, creationUser, BigDecimal.valueOf(42));
		CreationShare share2 = new CreationShare(creation, creationUser2, BigDecimal.valueOf(40));
		CreationShare share3 = new CreationShare(creation, creationUser3, BigDecimal.valueOf(18));
		testCreateShare(share);
		testCreateShare(share2);
		testCreateShare(share3);

		return CreationUtils.getCreationById(creation.getId());
	}

	public static Creation createCreationDataset(final User artist, final User publisher) throws Exception {

		final CreationUser creationUserArtist = CreationUtils.getCreationUserById(artist.getCreationDbId());
		final CreationUser creationUserPublisher = CreationUtils.getCreationUserById(publisher.getCreationDbId());

		Creation creation = new Creation("title"+System.currentTimeMillis(), creationUserArtist);
		creation = CreationDbTestUtils.testCreateCreation(creation);

		final BigDecimal shareQuotienArtist = BigDecimalUtils.getRandomQuotient();
		final BigDecimal shareQuotientPublisher = new BigDecimal(100.0).subtract(shareQuotienArtist);

		CreationShare shareArtist = new CreationShare(creation, creationUserArtist, shareQuotienArtist);
		shareArtist = CreationDbTestUtils.testCreateShare(shareArtist);

		CreationShare sharePublisher = new CreationShare(creation, creationUserPublisher, shareQuotientPublisher);
		sharePublisher = CreationDbTestUtils.testCreateShare(sharePublisher);

		creation.getShareSet().add(shareArtist);
		creation.getShareSet().add(sharePublisher);
		creation = CreationDbTestUtils.testCreateCreation(creation);

		return creation;
	}


}
