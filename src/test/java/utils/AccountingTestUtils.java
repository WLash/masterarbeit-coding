package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.laschinger.sawtooth.domain.entities.*;
import it.laschinger.sawtooth.request.accounting.AddCreditReq;
import it.laschinger.sawtooth.request.accounting.AddInvoiceReq;
import it.laschinger.sawtooth.request.accounting.AddPaymentReq;
import it.laschinger.sawtooth.utils.*;
import org.awaitility.Duration;
import org.awaitility.core.ConditionTimeoutException;
import org.bitcoinj.core.ECKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.protobuf.Transaction;
import testData.KeyUserTuple;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class AccountingTestUtils {

	private final static Logger logger = LoggerFactory.getLogger(AccountingTestUtils.class);

	public static Map<RoUserTuple, List<Fee>> mapFee(Map<RoUserTuple, List<Fee>> map, final User ro, final User user, final Fee fee){
		if(user != null){
			final RoUserTuple roUserTuple = new RoUserTuple(ro, user);
			map.computeIfAbsent(roUserTuple, k -> new ArrayList<>());
			final List<Fee> userFees = map.get(roUserTuple);
			userFees.add(fee);
			map.put(roUserTuple, userFees);
		}
		return map;
	}

	public static List<Invoice> createInvoices(final List<Fee> fees) throws Exception {

		final Map<RoUserTuple, List<Fee>> roDistFeeMap = new HashMap<>();
		BigDecimal amountFees = BigDecimal.ZERO;
		for(Fee fee : fees){
			final Tariff tariff = FeeUtils.getTariffOfFee(TestUtils.getApi(), fee);
			final User beneficiary = UserUtils.getUserByAddress(TestUtils.getApi(), fee.getBeneficiaryAddress());
			final User ro = beneficiary.getRole() == User.Role.ROYALTY_ORGANISATION ?
					beneficiary :
					UserUtils.getUserByAddress(TestUtils.getApi(), beneficiary.getAddressRoyaltyOrganisation());
			final User distributor = UserUtils.getUserByAddress(TestUtils.getApi(), tariff.getDistributorAddress());
			mapFee(roDistFeeMap, ro, distributor, fee);
			amountFees = amountFees.add(fee.getAmount());
		}

		final List<Invoice> invoices = new ArrayList<>();
		BigDecimal amountInvoices = BigDecimal.ZERO;
		logger.info("Tuples: " + roDistFeeMap.keySet());
		for(Map.Entry<RoUserTuple, List<Fee>> entry : roDistFeeMap.entrySet()){
			final String distributorAddress = AddressUtils.generateAddress(entry.getKey().getUser());
			final String roAddress = AddressUtils.generateAddress(entry.getKey().getRo());
			BigDecimal invoiceAmount = BigDecimal.ZERO;

			for(Fee fee : entry.getValue()){
				invoiceAmount = invoiceAmount.add(fee.getAmount());
			}

			final Invoice invoice = new Invoice("RG"+System.currentTimeMillis(), distributorAddress, roAddress,
					entry.getValue().stream().map(AddressUtils::generateAddress).collect(Collectors.toList()), Invoice.State.Open,
					 invoiceAmount);

			final ECKey keyRo = UserTestUtils.getUserByNameInMap(entry.getKey().getRo().getName(), UserTestUtils.roTestDataMap).getKey();
			assert addInvoice(invoice, keyRo);
			invoices.add(invoice);
			amountInvoices = amountInvoices.add(invoice.getAmount());
		}

		logger.info("AmountFees: " + amountFees);
		logger.info("AmountInvoices: " + amountInvoices);
		assert BigDecimalUtils.equalValue(amountFees, amountInvoices);

		return invoices;
	}

	public static List<Credit> createCredits(final List<Fee> fees) throws Exception {
		final Map<RoUserTuple, List<Fee>> roUserFeesMap = new HashMap<>();

		for(final Fee fee : fees){
			final User beneficiary = UserUtils.getUserByAddress(TestUtils.getApi(), fee.getBeneficiaryAddress());
			final User ro = beneficiary.getRole() == User.Role.ROYALTY_ORGANISATION ?
				beneficiary :
				UserUtils.getUserByAddress(TestUtils.getApi(), beneficiary.getAddressRoyaltyOrganisation());

			mapFee(roUserFeesMap, ro, beneficiary, fee);
		}

		final List<Credit> credits = new ArrayList<>();
		for(Map.Entry<RoUserTuple, List<Fee>> entry : roUserFeesMap.entrySet()){
			final User ro = entry.getKey().getRo();
			final User beneficiary = entry.getKey().getUser();
			final boolean isRoCredit = ro.equals(beneficiary);
			final List<Fee> beneficiaryFees = entry.getValue();
			BigDecimal creditAmount = BigDecimal.ZERO;
			logger.info("Fees: " + beneficiaryFees);
			for(final Fee fee : beneficiaryFees){
				creditAmount = creditAmount.add(fee.getAmount());
			}
			final List<String> feeAddresses = entry.getValue().stream().map(AddressUtils::generateAddress).collect(Collectors.toList());
			final Credit credit = new Credit("CR"+System.currentTimeMillis(), AddressUtils.generateAddress(ro), AddressUtils.generateAddress(beneficiary),
					creditAmount, feeAddresses);

			logger.info("Credit: " + credit);
			assert addCredit(credit, UserTestUtils.getDefaultRo().getKey());

			final User beneficiaryNew = UserUtils.getUserByUser(TestUtils.getApi(), beneficiary);
			final User roNew = UserUtils.getUserByUser(TestUtils.getApi(), ro);
			logger.info("Old-Balance: " + beneficiary.getBalance());
			logger.info("New-Balance: " + beneficiaryNew.getBalance());
			logger.info("Credit-Amount: " + creditAmount);
			logger.info("Difference: " + beneficiary.getBalance().subtract(creditAmount));
			if(isRoCredit){
				assert beneficiaryNew.getBalance().equals(roNew.getBalance());
			}else{
				assert beneficiary.getBalance().subtract(creditAmount).equals(beneficiaryNew.getBalance());
			}

//			assert roNew


		}
		return credits;
	}

	public static boolean createPayment(final Invoice invoice, final String originatorName) throws Exception {
		final KeyUserTuple roTuple = UserTestUtils.getUserByNameInMap(originatorName, UserTestUtils.roTestDataMap);
		return addPayment(invoice, roTuple.getKey());
	}

	public static boolean addPayment(final Invoice invoice, final ECKey keyRo) throws Exception {
		final AddPaymentReq request = new AddPaymentReq(Actions.ADD_ACTION, invoice.getInvoiceNo());
		final String payload = (new ObjectMapper().writeValueAsString(request));
		testAccounting(keyRo, payload);

		return testPaymentAdded(AddressUtils.generateAddress(invoice));
	}

	private static boolean testPaymentAdded(final String invoiceAddress) throws Exception {
		try {
			final AtomicBoolean bool = new AtomicBoolean(false);
			AwaitilityUtils.testAwaitilityUntilTrue(() -> {
				final Invoice newInvoice = new Invoice(StateUtils.getStateForAddressRest(TestUtils.getApi(), invoiceAddress).getData());
				logger.info("newInvoice: " + newInvoice);
				final boolean isStatePayed = newInvoice.getState() == Invoice.State.Payed;
				boolean feesArePayed = false;

				for (final String feeAddress : newInvoice.getFeeAddresses()) {
					final Fee fee = new Fee(StateUtils.getStateForAddressRest(TestUtils.getApi(), feeAddress).getData());
					logger.info("Fee: " + fee);
					feesArePayed = fee.getState() == Fee.State.PAYED;
					if (!feesArePayed) break;
				}
				logger.info("isStatePayed: " + isStatePayed);
				logger.info("feesArePayed: " + feesArePayed);
				bool.set(isStatePayed && feesArePayed);
				return bool.get();
			}, Duration.TEN_SECONDS);
			return bool.get();
		}catch (ConditionTimeoutException e ){
			e.printStackTrace();
			return false;
		}
	}

	private static boolean addInvoice(final Invoice invoice, final ECKey keyRo) throws Exception {
		final AddInvoiceReq request = new AddInvoiceReq(Actions.ADD_ACTION, invoice.getInvoiceNo(), invoice.getRecipientAddress(), invoice.getFeeAddresses());
		final String payload = new ObjectMapper().writeValueAsString(request);

		testAccounting(keyRo, payload);
		return TestUtils.testEntityCreated(invoice);
	}

	private static boolean addCredit(final Credit credit, final ECKey keyRo) throws Exception {
		final AddCreditReq request = new AddCreditReq(Actions.ADD_ACTION, credit.getCreditNo(), credit.getOriginatorAddress(), credit.getBeneficiaryAddress(), credit.getFeeAddresses());
		final String payload = new ObjectMapper().writeValueAsString(request);

		testAccounting(keyRo, payload);
		return TestUtils.testEntityCreated(credit);
	}

	private static void testAccounting(final ECKey key, final String payload){
		final Transaction txn = TransactionUtils.getTxnAccounting(key, payload);
		TestUtils.testRequestInSingleBatch(txn, key);
	}

	public static class RoUserTuple {
		final User ro;
		final User user;
		public RoUserTuple(final User ro, final User user){
			this.ro = ro;
			this.user = user;
		}

		public User getRo() {
			return ro;
		}

		public User getUser() {
			return user;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 31 * hash + ro.getPublicKeyHex().hashCode();
			hash = 31 * hash + user.getPublicKeyHex().hashCode();
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(obj instanceof RoUserTuple){
				final RoUserTuple tuple = (RoUserTuple) obj;
				return tuple.user.getPublicKeyHex().equals(this.user.getPublicKeyHex()) &&
					tuple.ro.getPublicKeyHex().equals(this.ro.getPublicKeyHex());
			}else{
				return false;
			}
		}

		@Override
		public String toString() {
			return "RoUserTuple{" +
					"ro=" + ro +
					", user=" + user +
					'}';
		}
	}
}
