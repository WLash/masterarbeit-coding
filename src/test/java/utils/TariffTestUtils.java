package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.request.tariff.AddTariffReq;
import it.laschinger.sawtooth.utils.Actions;
import it.laschinger.sawtooth.utils.AddressUtils;
import it.laschinger.sawtooth.utils.TransactionUtils;
import org.bitcoinj.core.ECKey;
import sawtooth.sdk.protobuf.Transaction;
import testData.KeyUserTuple;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TariffTestUtils {


	public static Tariff createTariff(final User distributor, final KeyUserTuple roTuple, final BigDecimal amount, final List<Transaction> transactions, final String... dependencies) throws Exception {
		final ECKey roKey = roTuple.getKey();
		final User ro = roTuple.getUser();

		final String distributorAddress = AddressUtils.generateAddress(distributor);
		final String roAddress = AddressUtils.generateAddress(ro);
		Tariff tariff = new Tariff(distributorAddress, roAddress, Usage.Type.DOWNLOAD, amount);
		tariff = addTariff(tariff, roKey, transactions, dependencies);
		return tariff;
	}


	public static Tariff addTariff(final Tariff tariff, final ECKey keyRo,final List<Transaction> transactions, String... dependencies) throws IOException{
		final AddTariffReq req = new AddTariffReq(Actions.ADD_ACTION, tariff.getDistributorAddress(), tariff.getRoyaltyOrganisationAddress(), Usage.Type.DOWNLOAD, tariff.getAmount());
		final String payload = new ObjectMapper().writeValueAsString(req);
		final Transaction txn = createTariffTransaction(keyRo, payload, Arrays.stream(dependencies).collect(Collectors.toList()));
		transactions.add(txn);
		tariff.setTransactionHeader(txn.getHeaderSignature());
		TestUtils.testRequestInSingleBatch(txn, keyRo);
		return tariff;
	}

	private static Transaction createTariffTransaction(final ECKey key, final String payload, final List<String> dependencies){
		final Transaction txn = TransactionUtils.getTxnTariff(key, payload, dependencies);
		return txn;
	}
}
