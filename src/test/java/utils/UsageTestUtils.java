package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.laschinger.sawtooth.domain.creation.Creation;
import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.request.usage.AddUsageRequest;
import it.laschinger.sawtooth.utils.Actions;
import it.laschinger.sawtooth.utils.AddressUtils;
import it.laschinger.sawtooth.utils.TransactionUtils;
import org.bitcoinj.core.ECKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.protobuf.Transaction;
import testData.KeyUserTuple;

import java.util.ArrayList;
import java.util.List;

public class UsageTestUtils {

	private final static Logger logger = LoggerFactory.getLogger(UsageTestUtils.class);

	public static Transaction addUsageWithoutTest(final Usage usage, final ECKey keyDistributor, final List<String> dependencies) throws JsonProcessingException {
		final AddUsageRequest req = new AddUsageRequest(Actions.ADD_ACTION, usage.getTariffAddress(),
			usage.getCreationId(), usage.getTime() , usage.getText());
		final String payload = new ObjectMapper().writeValueAsString(req);

		return testUsageRequest(keyDistributor, payload, dependencies);
	}

	public static List<Transaction> addUsagesWithoutTest(final List<Usage> usages, final ECKey keyDist, final List<String> dependencies) throws JsonProcessingException {
		final List<Transaction> txns = new ArrayList<>();
		for(Usage usage : usages){
			final AddUsageRequest req = new AddUsageRequest(Actions.ADD_ACTION, usage.getTariffAddress(),
				usage.getCreationId(), usage.getTime(), usage.getText());
			final String payload = new ObjectMapper().writeValueAsString(req);
			final Transaction txn = TransactionUtils.getTxnUsage(keyDist, payload, dependencies);
			txns.add(txn);
			usage.setTransactionHeader(txn.getHeaderSignature());
		}
		TestUtils.testMultipleRequestsInSingleBatch(txns, keyDist);
		return txns;
	}

	private static Transaction testUsageRequest(final ECKey key, final String payload, List<String> dependencies){
		final Transaction txn = TransactionUtils.getTxnUsage(key, payload, dependencies);
		TestUtils.testRequestInSingleBatch(txn, key);
		return txn;
	}

	public static Usage createSpecifiedUsage(final Tariff tariff, final KeyUserTuple distributorTuple,
											 final KeyUserTuple roTuple, final Usage.Type usageType,
											 final Creation creation, final int nr,
											 final List<String> dependencies) throws Exception {
		final Usage usage = new Usage(AddressUtils.generateAddress(tariff), creation.getId(), System.currentTimeMillis(), "testtext" + nr);
		final Transaction txn = addUsageWithoutTest(usage, distributorTuple.getKey(), dependencies);
		usage.setTransactionHeader(txn.getHeaderSignature());
		return usage;
	}

	public static List<Usage> createSpecifiedUsages(final Tariff tariff, final KeyUserTuple distributorTuple,
													final KeyUserTuple roTuple, final Usage.Type usageType,
													final List<Creation> creations, final int amount,
													final List<String> dependencies) throws Exception {
		final Creation creation = TestUtils.getRandomElementOfList(creations);
		final List<Usage> usages = new ArrayList<>();
		for(int i = 0; i < amount; i++) {
			final Usage usage = new Usage(AddressUtils.generateAddress(tariff), creation.getId(), System.currentTimeMillis(), "testtext" + i);
			usages.add(usage);
		}
		addUsagesWithoutTest(usages, distributorTuple.getKey(), dependencies);
		return usages;
	}
}
