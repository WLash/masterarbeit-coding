import it.laschinger.sawtooth.utils.RestApi;
import json.BlockEntry;
import json.TransactionEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.TestUtils;

import java.util.List;

public class Test {

	private final static Logger logger = LoggerFactory.getLogger(Test.class);

	@org.junit.Test
	public void getTransaction() throws Exception{
		final List<TransactionEntry> entries = TestUtils.getAllTransactions(new RestApi("sawtooth2", null, "webapps"));
		logger.info("Transactions: " + entries);
	}

	@org.junit.Test
	public void getBlocks() throws Exception{
		final List<BlockEntry> entries = TestUtils.getAllBlocks(new RestApi("sawtooth4", null, "webapps"));

		entries.forEach(bl -> {
			logger.info("Block: ");
			bl.getBatches().forEach(ba -> {
				logger.info("Batch" + ba);
				ba.getTransactions().forEach(ta -> logger.info("Ta: "+ ta));
			});
		});
	}
}
