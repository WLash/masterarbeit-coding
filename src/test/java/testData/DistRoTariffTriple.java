package testData;

import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.User;

public class DistRoTariffTriple {
	private User dist;
	private User ro;
	private Tariff tariff;

	public DistRoTariffTriple(User dist, User ro, Tariff tariff) {
		this.dist = dist;
		this.ro = ro;
		this.tariff = tariff;
	}

	public User getDist() {
		return dist;
	}

	public User getRo() {
		return ro;
	}

	public Tariff getTariff() {
		return tariff;
	}

	public boolean equalsDistAndRo(User dist, User ro){
		return this.dist.equals(dist) && this.ro.equals(ro);
	}
}
