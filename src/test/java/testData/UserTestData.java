package testData;

import it.laschinger.sawtooth.domain.entities.User;
import org.bitcoinj.core.ECKey;

public class UserTestData{
	private ECKey key;
	private User.Role role;

	public UserTestData(final ECKey key, final User.Role role){
		this.key = key;
		this.role = role;
	}

	public ECKey getKey() {
		return key;
	}

	public void setKey(ECKey key) {
		this.key = key;
	}

	public User.Role getRole() {
		return role;
	}

	public void setRole(User.Role role) {
		this.role = role;
	}

}