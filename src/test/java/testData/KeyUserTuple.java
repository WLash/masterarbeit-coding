package testData;

import it.laschinger.sawtooth.domain.entities.User;
import org.bitcoinj.core.ECKey;


public class KeyUserTuple {
	final ECKey key;
	final User user;

	public KeyUserTuple(final ECKey key, final User user){
		this.key = key;
		this.user = user;
	}

	public ECKey getKey() {
		return key;
	}

	public User getUser() {
		return user;
	}

	@Override
	public String toString() {
		return "KeyUserTuple{" +
				"key=" + key +
				", user=" + user +
				'}';
	}
}
