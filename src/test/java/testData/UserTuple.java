package testData;

import it.laschinger.sawtooth.domain.entities.User;

public  class UserTuple{
	private User user1;
	private User user2;

	public UserTuple(final User user1, final User user2){
		this.user1 = user1;
		this.user2 = user2;
	}

	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + user1.hashCode();
		hash = 31 * hash + user2.hashCode();
		return hash;
	}

	public User getUser1() {
		return user1;
	}

	public User getUser2() {
		return user2;
	}
}
