import com.mashape.unirest.http.exceptions.UnirestException;
import it.laschinger.sawtooth.domain.creation.Creation;
import it.laschinger.sawtooth.domain.creation.CreationShare;
import it.laschinger.sawtooth.domain.creation.CreationUser;
import it.laschinger.sawtooth.utils.CreationUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.processor.exceptions.InternalError;
import utils.CreationDbTestUtils;

import java.io.IOException;
import java.math.BigDecimal;

public class CreationDbTest {

	private final static Logger logger = LoggerFactory.getLogger(CreationDbTest.class);


	@Test
	public void testCreationDb() throws InternalError {

		final String userId = "1";
		final String  shareId = "1";
		final String creationId = "1";

		final CreationUser user = CreationUtils.getCreationUserById(userId);
		logger.info("CreationUser: " + user);

		final CreationShare share = CreationUtils.getCreationShareById(shareId);
		logger.info("CreationShare: " + share);

		final Creation creation = CreationUtils.getCreationById(creationId);
		logger.info("Creation: " + creation);

		if(! user.getId().equals(userId) || !share.getId().equals(shareId) || !creation.getId().equals(creationId)){
			throw new InternalError("Test Creation_DB incorrect!");
		}

	}

	@Test
	public void addUser() throws IOException, UnirestException {
		final Long timeStamp = System.currentTimeMillis();
		CreationUser user1 = new CreationUser();
		user1.setId("-1");
		user1.setName("TestUser" + timeStamp);
		user1 = CreationDbTestUtils.testCreateUser(user1);

		CreationUser user2 = new CreationUser();
		user2.setId("-1");
		user2.setName("TestUser2" + timeStamp);
		user2 = CreationDbTestUtils.testCreateUser(user2);

		Creation creation = new Creation();
		creation.setTitle("Title " + timeStamp);
		creation.setOwner(user1);
		creation = CreationDbTestUtils.testCreateCreation(creation);

		CreationShare share1 = new CreationShare();
		share1.setQuotient(BigDecimal.valueOf(42));
		share1.setShareHolder(user1);
		share1.setCreation(creation);
		share1 = CreationDbTestUtils.testCreateShare(share1);

		CreationShare share2 = new CreationShare();
		share2.setQuotient(BigDecimal.valueOf(58));
		share2.setShareHolder(user2);
		share2.setCreation(creation);
		share2 = CreationDbTestUtils.testCreateShare(share2);

		creation = CreationUtils.getCreationById(creation.getId());

		logger.info("Creation: " + creation);

		assert creation.getShareSet().contains(share1);
		assert creation.getShareSet().contains(share2);

	}
}
