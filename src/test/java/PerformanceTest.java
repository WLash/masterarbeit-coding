import com.fasterxml.jackson.core.JsonProcessingException;
import it.laschinger.sawtooth.domain.creation.Creation;
import it.laschinger.sawtooth.domain.entities.Tariff;
import it.laschinger.sawtooth.domain.entities.Usage;
import it.laschinger.sawtooth.domain.entities.User;
import it.laschinger.sawtooth.utils.AddressUtils;
import it.laschinger.sawtooth.utils.AwaitilityUtils;
import it.laschinger.sawtooth.utils.RestApi;
import json.BlockEntry;
import org.awaitility.Duration;
import org.bitcoinj.core.ECKey;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sawtooth.sdk.protobuf.Transaction;
import testData.DistRoTariffTriple;
import testData.KeyUserTuple;
import testData.UserTuple;
import utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


public class PerformanceTest {

	private final static Logger logger = LoggerFactory.getLogger(PerformanceTest.class);

	@Before
	public void init()throws Exception{
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ROYALTY_ORGANISATION_1);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ROYALTY_ORGANISATION_2);
//
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.DISTRIBUTOR_1);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.DISTRIBUTOR_2);
//
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.PUBLISHER_1);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.PUBLISHER_2);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.PUBLISHER_3);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.PUBLISHER_4);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.PUBLISHER_5);
//
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ARTIST_1);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ARTIST_2);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ARTIST_3);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ARTIST_4);
//		UserTestUtils.initUserByName(TestUtils.getApi(), UserTestUtils.ARTIST_5);
	}


	@Test
	public void initTest() throws Exception{
		List<Transaction> transactions = new ArrayList<>();
		final KeyUserTuple tupleRo1 = UserTestUtils.initUserByUserAndRoName(UserTestUtils.ROYALTY_ORGANISATION_1, null, true, transactions);
		final User userRo1 = tupleRo1.getUser();
		final ECKey keyRo1 = tupleRo1.getKey();

		final KeyUserTuple tupleRo2 = createInitUser(UserTestUtils.ROYALTY_ORGANISATION_2, tupleRo1, transactions);
		final KeyUserTuple tupleDist1 = createInitUser(UserTestUtils.DISTRIBUTOR_1, tupleRo1, transactions);
		final KeyUserTuple tupleDist2 = createInitUser(UserTestUtils.DISTRIBUTOR_2, tupleRo1, transactions);
		final KeyUserTuple tupleArtist1 = createInitUser(UserTestUtils.ARTIST_1, tupleRo1, transactions);
		final KeyUserTuple tupleArtist2 = createInitUser(UserTestUtils.ARTIST_2, tupleRo1, transactions);
		final KeyUserTuple tupleArtist3 = createInitUser(UserTestUtils.ARTIST_3, tupleRo1, transactions);
		final KeyUserTuple tupleArtist4 = createInitUser(UserTestUtils.ARTIST_4, tupleRo1, transactions);
		final KeyUserTuple tupleArtist5 = createInitUser(UserTestUtils.ARTIST_5, tupleRo1, transactions);
		final KeyUserTuple tuplePublisher1 = createInitUser(UserTestUtils.PUBLISHER_1, tupleRo1, transactions);
		final KeyUserTuple tuplePublisher2 = createInitUser(UserTestUtils.PUBLISHER_2, tupleRo1, transactions);
		final KeyUserTuple tuplePublisher3 = createInitUser(UserTestUtils.PUBLISHER_3, tupleRo1, transactions);
		final KeyUserTuple tuplePublisher4 = createInitUser(UserTestUtils.PUBLISHER_4, tupleRo1, transactions);
		final KeyUserTuple tuplePublisher5 = createInitUser(UserTestUtils.PUBLISHER_5, tupleRo1, transactions);

		Thread.sleep(10000);
		confirmInitUser(tupleRo2.getUser(), tupleRo1);
		confirmInitUser(tupleDist1.getUser(), tupleRo1);
		confirmInitUser(tupleDist2.getUser(), tupleRo1);
		confirmInitUser(tupleArtist1.getUser(), tupleRo1);
		confirmInitUser(tupleArtist2.getUser(), tupleRo1);
		confirmInitUser(tupleArtist3.getUser(), tupleRo1);
		confirmInitUser(tupleArtist4.getUser(), tupleRo1);
		confirmInitUser(tupleArtist5.getUser(), tupleRo1);
		confirmInitUser(tuplePublisher1.getUser(), tupleRo1);
		confirmInitUser(tuplePublisher2.getUser(), tupleRo1);
		confirmInitUser(tuplePublisher3.getUser(), tupleRo1);
		confirmInitUser(tuplePublisher4.getUser(), tupleRo1);
		confirmInitUser(tuplePublisher5.getUser(), tupleRo1);

		Thread.sleep(10000);
		final Tariff dist1Ro1 = createTariff(tupleDist1, tupleRo1, transactions);
		final Tariff dist1Ro2 = createTariff(tupleDist1, tupleRo2, transactions);
		final Tariff dist2Ro1 = createTariff(tupleDist2,  tupleRo1, transactions);
		final Tariff dist2Ro2 = createTariff(tupleDist2, tupleRo2, transactions);
		Thread.sleep(10000);


		logger.debug("Transactions: " +transactions);

		final List<DistRoTariffTriple> tripleList = new ArrayList<>();
		tripleList.add(new DistRoTariffTriple(tupleDist1.getUser(), tupleRo1.getUser(), dist1Ro1));
		tripleList.add(new DistRoTariffTriple(tupleDist1.getUser(), tupleRo2.getUser(), dist1Ro2));
		tripleList.add(new DistRoTariffTriple(tupleDist2.getUser(), tupleRo1.getUser(), dist2Ro1));
		tripleList.add(new DistRoTariffTriple(tupleDist2.getUser(), tupleRo2.getUser(), dist2Ro2));

		final List<UserTuple> userPublisherList = new ArrayList<>();
		userPublisherList.add(new UserTuple(tupleArtist1.getUser(), tuplePublisher1.getUser()));
		userPublisherList.add(new UserTuple(tupleArtist2.getUser(), tuplePublisher2.getUser()));
		userPublisherList.add(new UserTuple(tupleArtist3.getUser(), tuplePublisher3.getUser()));
		userPublisherList.add(new UserTuple(tupleArtist4.getUser(), tuplePublisher4.getUser()));
		userPublisherList.add(new UserTuple(tupleArtist5.getUser(), tuplePublisher5.getUser()));



		final List<Creation> creations = createCreations(userPublisherList);

		final long startTime = System.currentTimeMillis();
		final List<Usage> usages = new ArrayList<>();
		for(int i = 0; i<50; i++){
			final long sendUsageStartTime = System.currentTimeMillis();
			logger.info("USAGE " + i);
			final KeyUserTuple tupleDist = TestUtils.getRandomBoolean() ? tupleDist1 : tupleDist2;
			final KeyUserTuple tupleRo = TestUtils.getRandomBoolean() ? tupleRo1 : tupleRo2;
			final DistRoTariffTriple triple = tripleList.stream().filter(t -> t.equalsDistAndRo(tupleDist.getUser(), tupleRo.getUser())).findFirst().orElse(null);
			assert triple != null;
			final Tariff tariff = triple.getTariff();
//			usages.add(createUsage(triple.getTariff(), tupleDist, tupleRo, creations, i));
			usages.addAll(createUsages(triple.getTariff(), tupleDist, tupleRo, creations, 10));
			Thread.sleep(5000);
			logger.info("TimeForUsage: " + (System.currentTimeMillis() - sendUsageStartTime));
		}

		final Usage lastUsage = usages.get(usages.size()-1);
		logger.info("LastUsage: " + lastUsage);
		logger.info("LU-TH: " + lastUsage.getTransactionHeader());
		logger.info("LU-Address: " + AddressUtils.generateAddress(lastUsage));
		final long usagesFired = (System.currentTimeMillis()-startTime)/1000;
		logger.info("USAGES FIRED TIME " + usagesFired);

		final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
		AwaitilityUtils.testAwaitilityUntilTrue(() -> {
			logger.info("SEARCH FOR RIGHT BLOCK!");
			try {
				Thread.sleep(10000);
//				createUsage(dist1Ro1, tupleDist1, tupleRo1, creations, 13);
			} catch (Exception e) {
				e.printStackTrace();
			}

			final List<String> foundBlocks = new ArrayList<>();
				final List<RestApi> apis = TestUtils.getAllApis();
				for (RestApi api : apis) {
					final List<BlockEntry> block = TestUtils.getAllBlocks(api);
					final BlockEntry blockEntry = block.stream().filter(b -> b.getBatches().stream().anyMatch(ba -> ba.getTransactions().stream().anyMatch(ta -> ta.getHeaderSignature().equals(lastUsage.getTransactionHeader())))).findFirst().orElse(null);
					logger.info("BlockEntries: " + block);
					logger.info("BlockEntry found in api " + api + ": " + (blockEntry == null ? null : blockEntry.getHeaderSignature()));
					if (blockEntry != null) foundBlocks.add(blockEntry.getHeaderSignature());
				}
				logger.info("size: " + (foundBlocks.size() == apis.size()));
				if(!foundBlocks.isEmpty())	logger.info("frequency0: " + Collections.frequency(foundBlocks, foundBlocks.get(0)));
				if(foundBlocks.size() > 1) logger.info("frequency1: " + Collections.frequency(foundBlocks, foundBlocks.get(1)));
				final boolean isValid =  foundBlocks.size()>=3 &&
						((Collections.frequency(foundBlocks, foundBlocks.get(0)) >= 3 && foundBlocks.get(0) != null) ||
					(Collections.frequency(foundBlocks, foundBlocks.get(1)) >= 3 && foundBlocks.get(1) != null));

				atomicBoolean.set(isValid);
				return atomicBoolean.get();

				}, new Duration(60L, TimeUnit.MINUTES));
		logger.info("AwaitilityEnded After: " + + (System.currentTimeMillis() - startTime)/1000);
		assert atomicBoolean.get();
		logger.info("SUCCESSFULLY ENDED AFTER " + (System.currentTimeMillis() - startTime)/1000);
		logger.info("USAGES FIRED AT: " + usagesFired);

	}

	public Usage createUsage(final Tariff tariff, KeyUserTuple tupleDist, KeyUserTuple tupleRo, List<Creation> creations, int no) throws Exception{
		final Usage usage = UsageTestUtils.createSpecifiedUsage(tariff, tupleDist, tupleRo,	Usage.Type.DOWNLOAD,
			TestUtils.getRandomElementOfList(creations), no, Collections.singletonList(tariff.getTransactionHeader()));
		return usage;
	}

	public List<Usage> createUsages(final Tariff tariff, KeyUserTuple tupleDist, KeyUserTuple tupleRo, List<Creation> creations, int count) throws Exception {
		final List<Usage> usages = new ArrayList<>();
		usages.addAll(UsageTestUtils.createSpecifiedUsages(tariff, tupleDist, tupleRo, Usage.Type.DOWNLOAD, creations, count, Collections.singletonList(tariff.getTransactionHeader())));
		return usages;
	}

	public KeyUserTuple createInitUser(final String name, final KeyUserTuple tupleRo, final List<Transaction> transactions) throws Exception{
		final KeyUserTuple tupleUser = UserTestUtils.initUserByUserAndRoName(name, tupleRo.getUser(),false, transactions, tupleRo.getUser().getTransactionHeader());
		final User user = tupleUser.getUser();

		Thread.sleep(1000);
		return tupleUser;
	}

	public void confirmInitUser(final User user, final KeyUserTuple tupleRo) throws JsonProcessingException, InterruptedException {
		final Transaction confirmTxn = UserTestUtils.createUserConfirmTransaction(user, tupleRo.getUser(), tupleRo.getKey(), tupleRo.getUser().getTransactionHeader(), user.getTransactionHeader());
		user.setTransactionHeader(confirmTxn.getHeaderSignature());
		TestUtils.testRequestInSingleBatch(confirmTxn, tupleRo.getKey());
		Thread.sleep(1000);
	}

	public Tariff createTariff(final KeyUserTuple tupleDist, final KeyUserTuple tupleRo, final List<Transaction> transactions) throws Exception {
		final User dist = tupleDist.getUser();
		final User ro = tupleRo.getUser();
		final Tariff tariff = TariffTestUtils.createTariff(dist, tupleRo, BigDecimal.ONE, transactions, dist.getTransactionHeader(), ro.getTransactionHeader());
		Thread.sleep(1000);
		return tariff;
	}

	public List<Creation> createCreations(final List<UserTuple> userTuples) throws Exception{

		final List<Creation> creations = new ArrayList<>();
		userTuples.forEach(ut -> {

			try {
				final Creation creation = CreationDbTestUtils.createCreationDataset(ut.getUser1(), ut.getUser2());
				creations.add(creation);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return creations;
	}
}
